/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.onefi.utils.openSSL;

/**
 *
 * @author nonami
 */
public class OpenSslException extends RuntimeException {

    public OpenSslException(String message) {
        super(message);
    }

    public OpenSslException(Throwable cause) {
        super(cause);
    }

    public OpenSslException(String message, Throwable cause) {
        super(message, cause);
    }
}
