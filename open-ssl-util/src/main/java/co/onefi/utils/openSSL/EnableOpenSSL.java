package co.onefi.utils.openSSL;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 *
 * @author chizitere
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(OpenSslUtilConfiguration.class)
@Documented
public @interface EnableOpenSSL {
    String value() default "";
}