/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.onefi.utils.openSSL;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author nonami
 */
@Configuration
public class OpenSslUtilConfiguration {
    
    @Bean
    public OpenSslFactory openSSLUtilFactory() {
        return new OpenSslFactory();
    }
    
    @Bean
    public OpenSSLUtil openSslUtil(OpenSslFactory factory, @Value("${security.custom.privateKey:}") String privateKey, @Value("${security.custom.password:}") String password) {
        // For backward compatibility
        String identifier = factory.build(privateKey, password);
        return factory.getUtil(identifier);
    }
}
