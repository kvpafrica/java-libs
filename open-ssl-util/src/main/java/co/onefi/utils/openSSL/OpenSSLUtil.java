package co.onefi.utils.openSSL;

import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.EncryptionException;
import org.bouncycastle.openssl.PEMEncryptedKeyPair;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JcePEMDecryptorProviderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import java.io.*;
import java.security.KeyPair;
import java.security.Security;
import java.security.Signature;
import java.util.Base64;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 *
 * @author chizitere
 */
@Slf4j
public class OpenSSLUtil {

    private final KeyPair keyPair;

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    @Autowired
    public OpenSSLUtil(String privateKeyPath, String password) throws IOException {
        keyPair = readKeyPair(privateKeyPath, password);
    }

    private InputStream resolvePath(String path) throws IOException {
        if (new File(path).exists()) {
            return new FileInputStream(path);
        }

        InputStream is = getClass().getClassLoader().getResourceAsStream(path);
        if (is == null) {
            throw new FileNotFoundException("Could not load the Private key from: " + path);
        }

        return is;
    }

    public String encrypt(String text) {
        try {
            Cipher rsa = Cipher.getInstance("RSA");
            rsa.init(Cipher.ENCRYPT_MODE, keyPair.getPublic());
            return Base64.getEncoder().encodeToString(rsa.doFinal(text.getBytes()));
        } catch (Exception e) {
            log.error("Could not encrypt data: ", e);
        }
        return null;
    }

    public String sign(String text) {
        try {
            Signature privateSignature = Signature.getInstance("SHA256withRSA");
            privateSignature.initSign(keyPair.getPrivate());
            privateSignature.update(text.getBytes(UTF_8));

            byte[] signature = privateSignature.sign();

            return Base64.getEncoder().encodeToString(signature);
        } catch (Exception e) {
            log.error("Could not sign data: ", e);
        }
        return null;
    }

    public String decrypt(byte[] buffer) {
        try {
            Cipher rsa = Cipher.getInstance("RSA");
            rsa.init(Cipher.DECRYPT_MODE, keyPair.getPrivate());
            byte[] utf8 = rsa.doFinal(buffer);
            return new String(utf8, UTF_8);
        } catch (Exception e) {
            log.error("Could not decrypt data: ", e);
        }
        return null;
    }

    public String decrypt(String encrypted) {
        return decrypt(Base64.getDecoder().decode(encrypted));
    }

    private KeyPair readKeyPair(String privateKeyPath, String keyPassword) throws IOException {
        KeyPair kp = null;
        InputStreamReader reader = new InputStreamReader(resolvePath(privateKeyPath));

        try (PEMParser parser = new PEMParser(reader)) {
            final Object object = parser.readObject();
            final JcaPEMKeyConverter pemConverter = new JcaPEMKeyConverter();
            pemConverter.setProvider(BouncyCastleProvider.PROVIDER_NAME);

            if (object instanceof PEMEncryptedKeyPair) {
                final PEMEncryptedKeyPair encryptedKeyPair = (PEMEncryptedKeyPair) object;
                JcePEMDecryptorProviderBuilder decryptorBuilder = new JcePEMDecryptorProviderBuilder();
                decryptorBuilder.setProvider(BouncyCastleProvider.PROVIDER_NAME);
                kp = pemConverter.getKeyPair(encryptedKeyPair.decryptKeyPair(decryptorBuilder.build(keyPassword.toCharArray())));
            } else if (object instanceof PEMKeyPair) {
                kp = pemConverter.getKeyPair((PEMKeyPair) object);
            } else {
                log.debug("Expected PEMEncryptedKeyPair or PEMKeyPair, got: {}", object);
            }
        } catch (EncryptionException e) {
            throw e;
        }

        if (kp == null) {
            throw new IOException("Could not read key pair from: " + privateKeyPath);
        }

        return kp;
    }
}
