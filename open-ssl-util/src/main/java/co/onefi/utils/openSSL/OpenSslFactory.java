/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.onefi.utils.openSSL;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 *
 * @author nonami
 */
public class OpenSslFactory {

    Map<String, OpenSSLUtil> objects = new HashMap<>();

    String getAlphaNumericString(int n) {

        // length is bounded by 256 Character 
        byte[] array = new byte[256];
        new Random().nextBytes(array);

        String randomString
            = new String(array, Charset.forName("UTF-8"));

        // Create a StringBuffer to store the result 
        StringBuffer r = new StringBuffer();

        // Append first 20 alphanumeric characters 
        // from the generated random String into the result 
        for (int k = 0; k < randomString.length(); k++) {

            char ch = randomString.charAt(k);

            if (((ch >= 'a' && ch <= 'z')
                || (ch >= 'A' && ch <= 'Z')
                || (ch >= '0' && ch <= '9'))
                && (n > 0)) {

                r.append(ch);
                n--;
            }
        }

        // return the resultant string 
        return r.toString();
    }

    public String build(String privateKey, String password) {
        String identifier = getAlphaNumericString(15);
        try {
            objects.put(identifier, new OpenSSLUtil(privateKey, password));
            return identifier;
        } catch (IOException e) {
            throw new OpenSslException("Could not register private key", e);
        }
    }

    public OpenSSLUtil getUtil(String identifier) {
        if (objects.containsKey(identifier)) {
            return objects.get(identifier);
        }
        throw new OpenSslException("No util has been built for this identifier");
    }

}
