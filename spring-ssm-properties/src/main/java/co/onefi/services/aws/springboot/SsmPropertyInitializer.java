package co.onefi.services.aws.springboot;

import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagement;
import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagementClientBuilder;
import com.amazonaws.services.simplesystemsmanagement.model.GetParametersRequest;
import com.amazonaws.services.simplesystemsmanagement.model.GetParametersResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.cloud.bootstrap.config.PropertySourceLocator;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 *
 * @author taiwo
 */
@Slf4j
@Configuration
@AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE)
public class SsmPropertyInitializer implements PropertySourceLocator {

    private final int MAX_PARAMS_LENGTH = 10;
    private final Set<String> initialized = new HashSet<>();
    private final AWSSimpleSystemsManagement awsClient;

    public SsmPropertyInitializer() {
        System.out.println("Starting SSM Configurer");
        log.debug("SSM configurer...");
        awsClient = AWSSimpleSystemsManagementClientBuilder.defaultClient();
    }

    @Override
    public PropertySource<?> locate(Environment environment) {
        Map<String, Object> newSource = new HashMap<>();
        log.debug("Loaded SSM Configurer");
        AbstractEnvironment env = (AbstractEnvironment) environment;
        for (PropertySource propertySource : env.getPropertySources()) {
            if (propertySource instanceof MapPropertySource) {
                if (!initialized.contains(propertySource.getName())) {
                    processPropertySource((MapPropertySource) propertySource, newSource);
                    initialized.add(propertySource.getName());
                }
            }
        }

        //processing application.properties
        processPropertySource("application.properties", newSource);
        String[] profiles = env.getActiveProfiles();
        for (String profile : profiles) {
            processPropertySource("application-" + profile + ".properties", newSource);
        }

        return new MapPropertySource("onefi-ssm-configurer", newSource);
    }

    void processPropertySource(String propertyFile, Map<String, Object> ssmSource) {
        InputStream is = getClass().getClassLoader().getResourceAsStream(propertyFile);
        if (is != null) {
            Properties properties = new Properties();
            try {
                properties.load(is);
                Map<Object, Object> source = new HashMap<>();
                source.putAll(properties);
                processSource("classpath:" + propertyFile, source, ssmSource);
            } catch (IOException ex) {
                log.warn("Could not load properties from: " + propertyFile, ex);
            }
        } else {
            log.debug("Could not find properties file: " + propertyFile);
        }

    }

    private class BoolValueHolder {

        private boolean value;
    }

    private void processSource(String name, Map<Object, Object> source, Map<String, Object> ssmSource) {

        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        BoolValueHolder hasDecryption = new BoolValueHolder();
        source.forEach((k, v) -> {
            String value = String.valueOf(v);
            if (value.startsWith("ssm:")) {
                log.debug("Found SSM property: " + k);
                String ssmParts[] = value.replace("ssm:", "").split("~");
                log.debug("SSM parameter key " + k + " --> " + ssmParts[0]);

                if (!hasDecryption.value) {
                    hasDecryption.value = ssmParts.length > 1 && "true".equals(ssmParts[1]);
                }

                if(parameters.containsKey(ssmParts[0])){
                    parameters.add(ssmParts[0], k.toString());
                } else {
                    parameters.put(ssmParts[0], new ArrayList<String>(){{add(k.toString());}});
                }

            }
        });

        if (!parameters.isEmpty()) {
            log.debug("Property source: " + name + " has " + parameters.size() + " SSM parameters");

            List<String> paramNames = new ArrayList<>(parameters.keySet());
            for (int i = 0; i < paramNames.size(); i += MAX_PARAMS_LENGTH) {
                int endIndex = Math.min(paramNames.size(), i + MAX_PARAMS_LENGTH);
                GetParametersRequest gpr = new GetParametersRequest()
                    .withWithDecryption(hasDecryption.value)
                    .withNames(paramNames.subList(i, endIndex));

                GetParametersResult result = awsClient.getParameters(gpr);

                result.getParameters().forEach((p) -> {
                    Collection<String> propNames = parameters.get(p.getName());
                    propNames.forEach((propName) -> {
                        ssmSource.put(propName, p.getValue());
                        source.put(propName, p.getValue());
                    });
                });

                log.warn(name + " :: INVALID PARAMETERS: " + result.getInvalidParameters());
            }
        } else {
            log.debug("Property source: " + name + " has no SSM parameters");
        }
    }

    private void processPropertySource(MapPropertySource mapPropertySource, Map<String, Object> ssmSource) {
        Map<Object, Object> nSource = new HashMap<>();
        nSource.putAll(mapPropertySource.getSource());
        processSource(mapPropertySource.getName(), nSource, ssmSource);
    }
}
