# Java Libraries

This is a collection of Java libraries. They work with Spring Boot to enhance development. 

See individual packages for what each library is useful for. Publishing is done with Wagon GIt plugin.


## Adding New Libraries

Follow POM layout to add new libraries in this repo

## Publishing

```
cd library-path
mvn deploy
```

**Note**

If `mvn deploy` completes successfully, necessary commits will be made to the `releases_via_java-libs` branch. Raise a PR to merge it into the `releases` branch. 
Once merged, do a `fast-forward merge` from the `releases` branch into the `releases_via_java-libs` and `releases_via_wallet-service` branches. 

## Including in Your Project

To include these libraries in your project do the following:


```

<dependencies>
    <dependency>
        <groupId>group-id-of-library</groupId>
        <artifactId>artifact-id</artifactId>
        <version>{library-version}</version>
    </dependency>
<dependencies>


<repositories>
	<!-- other repos -->
	<repository>
            <id>onefi-bb</id>
            <name>OneFi Bitbucket</name>
	        <releases>
	                <enabled>true</enabled>
	        </releases>
	        <snapshots>
	                <enabled>false</enabled>
	        </snapshots>
            <url>https://api.bitbucket.org/2.0/repositories/kvpafrica/java-libs/src/releases</url>
	</repository>
	<!-- other repos -->
</repositories>
```