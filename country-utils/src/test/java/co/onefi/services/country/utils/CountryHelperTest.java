package co.onefi.services.country.utils;

import co.onefi.services.country.utils.beans.CountriesConfiguration;
import org.junit.jupiter.api.*;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author taiwo
 */
public class CountryHelperTest {

    CountriesConfiguration countriesConfig;
    CountryHelper instance;

    @BeforeEach
    public void setUp() {
        countriesConfig = new CountriesConfiguration();
        countriesConfig.setCountryConfig(new HashMap<>());
        CountriesConfiguration.Country countryConfig = new CountriesConfiguration.Country();
        countriesConfig.getCountryConfig().put("ng", countryConfig);
        countryConfig.setDiallingCode("234");
        countryConfig.setCurrencyCode("NGN");
        instance = new CountryHelper(countriesConfig);
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of formatPhoneNumber method, of class CountryHelper.
     */
    @Test
    public void testFormatPhoneNumber() {
        assertEquals("2348012345678", instance.formatPhoneNumber("ng", "2348012345678"));
        assertEquals("2348012345678", instance.formatPhoneNumber("ng", "+2348012345678"));
        assertEquals("2348012345678", instance.formatPhoneNumber("ng", "08012345678"));
        assertEquals("2348012345678", instance.formatPhoneNumber("ng", "8012345678"));
        assertEquals("+2348012345678", instance.formatPhoneNumber("ng", "8012345678", true));
    }

    /**
     * Test of isLocalCurrency method, of class CountryHelper.
     */
    @Test
    public void testIsLocalCurrency() {
        assertTrue(instance.isLocalCurrency("ng", "ngn"));
        assertTrue(instance.isLocalCurrency("ng", "NGN"));
        assertFalse(instance.isLocalCurrency("ng", "KES"));
        assertFalse(instance.isLocalCurrency("ng", "GHS"));
    }

}
