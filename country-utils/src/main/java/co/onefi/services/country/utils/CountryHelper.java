package co.onefi.services.country.utils;

import co.onefi.services.country.utils.beans.CountriesConfiguration;

/**
 *
 * @author taiwo
 */
public class CountryHelper {

    private final CountriesConfiguration countriesConfig;

    public CountryHelper(CountriesConfiguration countriesConfig) {
        this.countriesConfig = countriesConfig;
    }

    public String formatPhoneNumber(String country, String phoneNumber) {
        return formatPhoneNumber(country, phoneNumber, false);
    }

    public String formatPhoneNumber(String country, String phoneNumber, boolean prefixPlus) {
        CountriesConfiguration.Country countryConfig = getCountryConfig(country);

        if (phoneNumber.startsWith("+")) {
            phoneNumber = phoneNumber.replace("+", "");
        }

        if (phoneNumber.startsWith(countryConfig.getDiallingCode())) {
            return phoneNumber;
        }

        String formatted = countryConfig.getDiallingCode() + phoneNumber.replaceFirst("^0", "");
        return (prefixPlus ? "+" : "") + formatted;
    }

    private CountriesConfiguration.Country getCountryConfig(String country) {
        CountriesConfiguration.Country countryConfig = countriesConfig.getCountryConfig().get(country.toLowerCase());
        if (countryConfig == null) {
            throw new IllegalStateException("Country configuration not found");
        }

        return countryConfig;
    }

    public boolean isLocalCurrency(String country, String currencyCode) {
        CountriesConfiguration.Country countryConfig = getCountryConfig(country);
        return countryConfig.getCurrencyCode().equalsIgnoreCase(currencyCode);
    }

}
