package co.onefi.services.country.utils.localization.tools;

import co.onefi.services.country.utils.localization.LocalizationConfiguration;
import co.onefi.services.country.utils.localization.model.ConfigResponse;
import co.onefi.services.country.utils.localization.model.PushMessageResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RMapCache;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.StringUtils;

@Slf4j
@Component
public class CacheManager {

    @Autowired
    private RedissonClient client;
    private final String KEY_PREFIX = "L10N";
    private final String CACHE_NAME = "L10N_MESSAGES";
    private ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Autowired
    private LocalizationConfiguration configuration;


    private String generateKey(String transactionType, String country, String language, String mKey) {
        String key = String.format("%s-%s-%s", KEY_PREFIX, transactionType, country.toLowerCase());
        if (StringUtils.isNotEmpty(language)) {
            key += String.format("-%s", language);
        }
        if (StringUtils.isNotEmpty(mKey)) {
            key += String.format("-%s", mKey);
        }
        return key;
    }

    public PushMessageResponse getCachedPushMessage(String country, String language, String mKey) {
        String cacheKey = generateKey("push", country, language, mKey);
        RMapCache<String, String> cache = client.getMapCache(CACHE_NAME);
        String cachedMessage = cache.get(cacheKey);
        try {
            if (cachedMessage != null) {
                return OBJECT_MAPPER.readValue(cachedMessage, PushMessageResponse.class);
            }
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }
        return null;

    }

    public PushMessageResponse savePushMessage(String country, String language, String mKey, PushMessageResponse message) {
        String cacheKey = generateKey("push", country, language, mKey);
        try {
            String messageString = OBJECT_MAPPER.writeValueAsString(message);
            RMapCache<String, String> cache = client.getMapCache(CACHE_NAME);
            cache.fastPutIfAbsent(cacheKey, messageString, configuration.getCacheTtl(), TimeUnit.SECONDS);
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }

        return message;
    }

    public String getCachedMessage(String transactionType,String country, String language, String mKey) {
        String cacheKey = generateKey(transactionType, country, language, mKey);
        RMapCache<String, String> cache = client.getMapCache(CACHE_NAME);
        return cache.get(cacheKey);
    }


    public String saveMessage(String transactionType,String country, String language, String mKey, String message) {
        String cacheKey = generateKey(transactionType, country, language, mKey);
        RMapCache<String, String> cache = client.getMapCache(CACHE_NAME);
        cache.fastPutIfAbsent(cacheKey, message, configuration.getCacheTtl(), TimeUnit.SECONDS);
        return message;
    }

    public ConfigResponse getCachedConfig(String country) {
        String cacheKey = generateKey("config", country, null, null);
        RMapCache<String, String> cache = client.getMapCache(CACHE_NAME);
        String cachedMessage = cache.get(cacheKey);
        try {
            if (cachedMessage != null) {
                return OBJECT_MAPPER.readValue(cachedMessage, ConfigResponse.class);
            }
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }
        return null;
    }

    public ConfigResponse saveConfig(String country, ConfigResponse config) {
        String cacheKey = generateKey("config", country, null, null);
        try {
            String messageString = OBJECT_MAPPER.writeValueAsString(config);
            RMapCache<String, String> cache = client.getMapCache(CACHE_NAME);
            cache.fastPutIfAbsent(cacheKey, messageString, configuration.getCacheTtl(), TimeUnit.SECONDS);
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }

        return config;
    }
}
