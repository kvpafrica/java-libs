package co.onefi.services.country.utils.localization;

import lombok.Data;

@Data
public class LocalizationConfiguration {

    private String baseUrl;

    private String apiKey;

    private int cacheTtl;

    private boolean debugMode;
}
