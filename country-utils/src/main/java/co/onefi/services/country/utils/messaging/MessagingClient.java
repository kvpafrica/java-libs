/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.onefi.services.country.utils.messaging;

import co.onefi.services.country.utils.messaging.models.MessagingPushRequest;
import co.onefi.services.country.utils.messaging.models.MessagingResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author nonami
 */
public interface MessagingClient {
    
    @PostMapping("/push")
    public MessagingResponse sendPush(MessagingPushRequest request);

    @PostMapping("/push?lang={lang}")
    public MessagingResponse sendPush(MessagingPushRequest request, @RequestParam("lang") String lang);

}
