/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.onefi.services.country.utils.messaging.models;

import java.util.Map;
import lombok.Data;

/**
 *
 * @author nonami
 */
@Data
public class MessagingPushRequest {
    String clientId;
    String messageKey;
    Map<String, String> vars;
    String action;
    String extraPayload;
}
