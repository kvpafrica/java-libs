/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.onefi.services.country.utils.messaging;

import co.onefi.services.country.utils.LocaleRequestInterceptor;
import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.Logger;
import feign.Retryer;
import feign.codec.Decoder;
import feign.codec.Encoder;
import feign.slf4j.Slf4jLogger;
import java.util.Map;
import java.util.WeakHashMap;
import lombok.Data;

/**
 *
 * @author nonami
 */
@Data
public class MessagingClientBuilder {

    final MessagingConfiguration config;
    Decoder decoder;
    Encoder encoder;
    Contract contract;

    private final Map<String, MessagingClient> apiClients = new WeakHashMap<>();

    public MessagingClientBuilder(MessagingConfiguration config) {
        this.config = config;
    }

    public MessagingClient getClient(String country) {
        country = country.toLowerCase();
        if (!apiClients.containsKey(country)) {
            apiClients.put(country, initFeignClient(country));
        }

        return apiClients.get(country);
    }

    MessagingClient initFeignClient(String country) {
        return Feign.builder()
            .client(new Client.Default(null, null))
            .encoder(encoder)
            .decoder(decoder)
            .logLevel(config.isDebugMode() ? Logger.Level.FULL : Logger.Level.BASIC)
            .contract(contract)
            .retryer(Retryer.NEVER_RETRY)
            .logger(new Slf4jLogger(MessagingClient.class.getName()))
            .requestInterceptor(new LocaleRequestInterceptor(country))
            .target(MessagingClient.class, config.getBaseUrl());
    }
}
