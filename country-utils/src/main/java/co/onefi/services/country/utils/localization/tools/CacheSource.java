package co.onefi.services.country.utils.localization.tools;

        import co.onefi.services.country.utils.localization.model.ConfigResponse;
        import co.onefi.services.country.utils.localization.model.PushMessageResponse;
        import org.springframework.beans.factory.annotation.Autowired;

public class CacheSource implements LocalizationDataSource {

    @Autowired
    private CacheManager cacheManager;

    @Override
    public PushMessageResponse getPushMessage(String country, String language, String key) {
        return cacheManager.getCachedPushMessage(country, language, key);
    }

    @Override
    public String getMessage(String translationsType, String country, String language, String key) {
        return cacheManager.getCachedMessage(translationsType, country, language, key);
    }

    @Override
    public ConfigResponse getConfig(String country) {
        return cacheManager.getCachedConfig(country);
    }
}
