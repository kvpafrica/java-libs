package co.onefi.services.country.utils.localization;

import co.onefi.services.country.utils.localization.model.ConfigResponse;
import co.onefi.services.country.utils.localization.model.PushMessageResponse;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.Map;

@Slf4j
public class LocalizationMessageService {

    @Autowired
    private LocalizationTemplateService localizationTemplateService;

    private Handlebars handlebars = new Handlebars();


    /**
     * Returns message title and content for push messages with the specified message key.
     *
     * @param country
     * @param language
     * @param key
     */
    public PushMessageResponse getPushMessage(String country, String language, String key, Map<String, String> messageVars) {
        PushMessageResponse pushMessageResponse = localizationTemplateService.getPushMessage(country, language, key);
        pushMessageResponse.setContent(resolveHandleBar(pushMessageResponse.getContent(), messageVars));
        return pushMessageResponse;
    }

    /**
     * Returns message title and content for messages with the specified message key.
     *
     * @param translationsType
     * @param country
     * @param language
     * @param key
     */
    public String getMessage(String translationsType, String country, String language, String key, Map<String, String> messageVars) {
        String messageResponse = localizationTemplateService.getMessage(translationsType, country, language, key);
        return resolveHandleBar(messageResponse, messageVars);
    }


    /**
     * Returns message configuration.
     *
     * @param country
     */
    public ConfigResponse getConfig(String country) {
        return localizationTemplateService.getConfig(country);
    }


    private String resolveHandleBar(String message, Map<String, String> messageVars) {
        try {
            Template template = handlebars.compileInline(message);
            return template.apply(messageVars);
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
            return message;
        }
    }

}
