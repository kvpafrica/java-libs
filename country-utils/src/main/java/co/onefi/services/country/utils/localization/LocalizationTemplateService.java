package co.onefi.services.country.utils.localization;

import co.onefi.services.country.utils.localization.model.ConfigResponse;
import co.onefi.services.country.utils.localization.model.PushMessageResponse;
import co.onefi.services.country.utils.localization.tools.LocalizationDataSource;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class LocalizationTemplateService {

    @Autowired
    List<LocalizationDataSource> dataSources;

    /**
     * Returns message title and content template for push messages with the specified message key.
     *
     * @param country
     * @param language
     * @param key
     */
    public PushMessageResponse getPushMessage(String country, String language, String key) {
        PushMessageResponse pushMessageResponse = null;
        for (LocalizationDataSource dataSource : dataSources) {
            PushMessageResponse response = dataSource.getPushMessage(country, language, key);
            if (response != null) {
                pushMessageResponse = response;
                break;
            }
        }
        return pushMessageResponse;
    }

    /**
     * Returns message title and content template for messages with the specified message key.
     *
     * @param translationsType
     * @param country
     * @param language
     * @param key
     */
    public String getMessage(String translationsType, String country, String language, String key) {
        String messageResponse = null;
        for (LocalizationDataSource dataSource : dataSources) {
            String response = dataSource.getMessage(translationsType, country, language, key);
            if (response != null) {
                messageResponse = response;
                break;
            }
        }
        return messageResponse;
    }

    /**
     * Returns message configuration.
     *
     * @param country
     */
    public ConfigResponse getConfig(String country) {
        ConfigResponse messageResponse = null;
        for (LocalizationDataSource dataSource : dataSources) {
            ConfigResponse response = dataSource.getConfig(country);
            if (response != null) {
                messageResponse = response;
                break;
            }
        }
        return messageResponse;
    }
}
