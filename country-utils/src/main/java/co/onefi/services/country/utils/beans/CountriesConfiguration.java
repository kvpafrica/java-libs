package co.onefi.services.country.utils.beans;

import java.util.Map;
import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

/**
 *
 * @author nonami
 */
@Data
@Validated
public class CountriesConfiguration {

    @Valid
    @NotNull
    private Map<String, Country> countryConfig;

    @Data
    public static class Country {

        @NotNull
        private String name;

        @NotNull
        private String code;

        private String currencyName;

        private String diallingCode;

        @NotNull
        private String currencyCode;

        private String timezone;

        private Set<String> networkProviders;

        private int phoneNumberLength;

        private boolean supportsBvn;
        
        private String localeId;

    }
}
