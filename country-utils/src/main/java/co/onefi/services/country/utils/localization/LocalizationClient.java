package co.onefi.services.country.utils.localization;

import co.onefi.services.country.utils.localization.model.ConfigResponse;
import co.onefi.services.country.utils.localization.model.PushMessageResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

public interface LocalizationClient {

    @GetMapping("/translations/{translationsType}/{key}")
    public String getTranslationMessage(@PathVariable("translationsType") String translationsType,
                                                     @PathVariable("key") String key, @RequestHeader("X-Entity") String country,
                                                     @RequestHeader("X-Lang") String language);


    @GetMapping("/translations/push/{key}")
    public PushMessageResponse getPushTranslationMessage(@PathVariable("key") String key, @RequestHeader("X-Entity") String country,
                                           @RequestHeader("X-Lang") String language);

    @GetMapping("/config")
    public ConfigResponse getConfig(@RequestHeader("X-Entity") String country);
}
