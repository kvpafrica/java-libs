package co.onefi.services.country.utils.localization;

import co.onefi.services.country.utils.localization.tools.*;
import feign.Contract;
import feign.codec.Decoder;
import feign.codec.Encoder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.ReplicatedServersConfig;
import org.redisson.config.SentinelServersConfig;
import org.redisson.config.SingleServerConfig;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.validation.annotation.Validated;

@Configuration
@Import(FeignClientsConfiguration.class)
@EnableConfigurationProperties(value = {RedisProperties.class})
@Slf4j
public class LocalizationApiConfiguration {

    @Bean
    @Validated
    @ConfigurationProperties("l10nService")
    public LocalizationConfiguration localizationServiceConfiguration() {
        return new LocalizationConfiguration();
    }


    @Bean
    public LocalizationClientBuilder localizationApiClient(
            LocalizationConfiguration config,
            Decoder decoder,
            Encoder encoder,
            Contract contract
    ) {
        LocalizationClientBuilder clientBuilder = new LocalizationClientBuilder(config);
        clientBuilder.setContract(contract);
        clientBuilder.setEncoder(encoder);
        clientBuilder.setDecoder(decoder);

        return clientBuilder;
    }


    @Bean
    public RedissonClient redissonClient(RedisProperties redisProperties) {
        Config config = new Config();
        if (redisProperties.getSentinel() != null) {
            if (StringUtils.isEmpty(redisProperties.getSentinel().getMaster())) {
                ReplicatedServersConfig serversConfig = config.useReplicatedServers();
                serversConfig.addNodeAddress(redisProperties.getSentinel().getNodes().toArray(String[]::new));
                if (redisProperties.getPassword() != null) {
                    serversConfig.setPassword(redisProperties.getPassword());
                }
                serversConfig.setConnectTimeout(redisProperties.getTimeout().toMillisPart());
            } else {
                SentinelServersConfig sentinelServersConfig = config.useSentinelServers();
                sentinelServersConfig.setMasterName(redisProperties.getSentinel().getMaster());
                if (redisProperties.getSentinel().getNodes() != null && !redisProperties.getSentinel().getNodes().isEmpty()) {
                    sentinelServersConfig.addSentinelAddress(redisProperties.getSentinel().getNodes().toArray(String[]::new));
                }
                sentinelServersConfig.setDatabase(redisProperties.getDatabase());
                if (redisProperties.getPassword() != null) {
                    sentinelServersConfig.setPassword(redisProperties.getPassword());
                }
            }
        } else {
            SingleServerConfig singleServerConfig = config.useSingleServer();
            String schema = redisProperties.isSsl() ? "rediss://" : "redis://";
            singleServerConfig.setAddress(schema + redisProperties.getHost() + ":" + redisProperties.getPort());
            singleServerConfig.setDatabase(redisProperties.getDatabase());
            if (redisProperties.getPassword() != null) {
                singleServerConfig.setPassword(redisProperties.getPassword());
            }
        }
        return Redisson.create(config);
    }

    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public LocalizationDataSource getCacheSource() {
        return new CacheSource();
    }

    @Bean
    @Order(Ordered.LOWEST_PRECEDENCE)
    public LocalizationDataSource getServiceSource() {
        return new ServiceSource();
    }

    @Bean
    public CacheManager getCacheManager() {
        return new CacheManager();
    }

    @Bean
    public LocalizationTemplateService localizationTemplateService() {
        return new LocalizationTemplateService();
    }

    @Bean
    public LocalizationMessageService localizationMessageService() {
        return new LocalizationMessageService();
    }

}
