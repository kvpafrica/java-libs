/*
 * Copyright (C) One Finance & Investments Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by OneFi Developers <developers@onefi.co>, 2019
 */
package co.onefi.services.country.utils;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import java.util.Locale;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Nonami
 */
public class LocaleRequestInterceptor implements RequestInterceptor {

    private final String entity;
    private final String localeLang;

    public LocaleRequestInterceptor(String entity) {
        this.entity = entity;
        this.localeLang = null;
    }
    
    public LocaleRequestInterceptor(Locale locale) {
        this.entity = locale.getCountry();
        this.localeLang = locale.getLanguage();
    }
    
    
    public LocaleRequestInterceptor(String entity, String language) {
        this.entity = entity;
        this.localeLang = language;
    }

    @Override
    public void apply(RequestTemplate template) {
        template.header("X-Entity", entity);
        
        if (!StringUtils.isEmpty(this.localeLang)) {
            template.header("X-Locale-Lang", this.localeLang);
        }
    }

}
