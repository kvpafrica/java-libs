/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.onefi.services.country.utils.messaging;

import feign.Contract;
import feign.codec.Decoder;
import feign.codec.Encoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.validation.annotation.Validated;

/**
 *
 * @author nonami
 */
@Configuration
@Import(FeignClientsConfiguration.class)
@Slf4j
public class MessagingApiConfiguration {

    @Bean
    @Validated
    @ConfigurationProperties("l10n-messaging")
    public MessagingConfiguration messagingServiceConfig() {
        return new MessagingConfiguration();
    }

    @Bean
    public MessagingClientBuilder messagingApiClient(
        MessagingConfiguration config,
        Decoder decoder,
        Encoder encoder,
        Contract contract
    ) {
        MessagingClientBuilder clientBuilder = new MessagingClientBuilder(config);
        clientBuilder.setContract(contract);
        clientBuilder.setEncoder(encoder);
        clientBuilder.setDecoder(decoder);
        
        return clientBuilder;
    }

}
