package co.onefi.services.country.utils.localization.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConfigResponse {

    private String name;
    private String code;
    private String dialingCode;
    private PhoneNumberFormatConfig phoneNumberFormat;
    private DateFormatConfig dateFormat;
    private LocationConfig location;
    private String defaultLanguage;
    private CurrencyConfig currency;
    private List<LanguageConfig> languages;


    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class PhoneNumberFormatConfig{
        private int minLength;
        private int maxLength;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class DateFormatConfig{
        private String java;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class LocationConfig{

        @JsonProperty("long")
        private Double longitude;
        @JsonProperty("lat")
        private Double latitude;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class CurrencyConfig{
        private String name;
        private String code;
        private String symbol;
        private String symbolHtmlEntity;
        private String symbolUnicodePoint;
        private String format;
        private SubCurrencyConfig subCurrency;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class SubCurrencyConfig{
        private String name;
        private Integer multiplier;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class LanguageConfig{
        private String name;
        private String code;
    }
}
