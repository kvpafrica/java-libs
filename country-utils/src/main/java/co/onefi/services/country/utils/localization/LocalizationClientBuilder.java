/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.onefi.services.country.utils.localization;

import co.onefi.services.country.utils.LocaleRequestInterceptor;
import feign.*;
import feign.codec.Decoder;
import feign.codec.Encoder;
import feign.slf4j.Slf4jLogger;
import lombok.Data;

import java.util.Map;
import java.util.WeakHashMap;

@Data
public class LocalizationClientBuilder {

    final LocalizationConfiguration config;
    Decoder decoder;
    Encoder encoder;
    Contract contract;

    private final Map<String, LocalizationClient> apiClients = new WeakHashMap<>();

    public LocalizationClientBuilder(LocalizationConfiguration config) {
        this.config = config;
    }

    public LocalizationClient getClient(String country) {
        country = country.toLowerCase();
        if (!apiClients.containsKey(country)) {
            apiClients.put(country, initFeignClient(country));
        }

        return apiClients.get(country);
    }

    LocalizationClient initFeignClient(String country) {
        return Feign.builder()
                .client(new Client.Default(null, null))
                .encoder(encoder)
                .decoder(decoder)
                .logLevel(config.isDebugMode() ? Logger.Level.FULL : Logger.Level.BASIC)
                .contract(contract)
                .retryer(Retryer.NEVER_RETRY)
                .logger(new Slf4jLogger(LocalizationClient.class.getName()))
                .requestInterceptor(new LocaleRequestInterceptor(country))
                .target(LocalizationClient.class, config.getBaseUrl());
    }
}
