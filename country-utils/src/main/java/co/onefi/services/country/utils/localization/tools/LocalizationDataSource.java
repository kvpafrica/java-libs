package co.onefi.services.country.utils.localization.tools;

import co.onefi.services.country.utils.localization.model.ConfigResponse;
import co.onefi.services.country.utils.localization.model.PushMessageResponse;

public interface LocalizationDataSource {
    /**
     * Returns message title and content for push messages with the specified message key.
     *
     * @param country
     * @param language
     * @param key
     */
    PushMessageResponse getPushMessage(String country, String language, String key);

    /**
     * Returns message title and content for sms messages with the specified message key.
     *
     * @param translationsType
     * @param country
     * @param language
     * @param key
     */
    String getMessage(String translationsType, String country, String language, String key);

    /**
     * Returns message configuration.
     *
     * @param country
     */
    ConfigResponse getConfig(String country);
}
