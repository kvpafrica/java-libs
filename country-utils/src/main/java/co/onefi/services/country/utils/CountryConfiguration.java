package co.onefi.services.country.utils;

import co.onefi.services.country.utils.beans.CountriesConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author taiwo
 */
@Configuration
public class CountryConfiguration {

    @Bean
    @ConfigurationProperties
    public CountriesConfiguration countriesConfig() {
        return new CountriesConfiguration();
    }

    @Bean
    @Autowired
    public CountryHelper countryHelper(CountriesConfiguration countriesConfig) {
        return new CountryHelper(countriesConfig);
    }
}
