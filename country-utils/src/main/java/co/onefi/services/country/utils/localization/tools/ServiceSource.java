package co.onefi.services.country.utils.localization.tools;

import co.onefi.services.country.utils.localization.LocalizationClientBuilder;
import co.onefi.services.country.utils.localization.model.ConfigResponse;
import co.onefi.services.country.utils.localization.model.PushMessageResponse;
import org.springframework.beans.factory.annotation.Autowired;


public class ServiceSource implements LocalizationDataSource {

    @Autowired
    LocalizationClientBuilder localizationApiClientBuilder;

    @Autowired
    private CacheManager cacheManager;

    @Override
    public PushMessageResponse getPushMessage(String country, String language, String key) {
        PushMessageResponse result = localizationApiClientBuilder.getClient(country).getPushTranslationMessage(key, country, language);
        return cacheManager.savePushMessage(country, language, key, result);
    }

    @Override
    public String getMessage(String translationsType, String country, String language, String key) {
        String result = localizationApiClientBuilder.getClient(country).getTranslationMessage(translationsType, key, country, language);
        return cacheManager.saveMessage(translationsType, country, language, key, result);
    }

    @Override
    public ConfigResponse getConfig(String country) {
        ConfigResponse result = localizationApiClientBuilder.getClient(country).getConfig(country);
        return cacheManager.saveConfig(country, result);
    }
}
