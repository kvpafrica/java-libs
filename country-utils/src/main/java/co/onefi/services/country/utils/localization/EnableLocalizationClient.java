/*
 * Copyright (C) One Finance & Investments Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by OneFi Developers <developers@onefi.co>, 2018
 */
package co.onefi.services.country.utils.localization;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 *
 * @author tomachi
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import({LocalizationApiConfiguration.class})
public @interface EnableLocalizationClient {

    String value() default "";
    
}
