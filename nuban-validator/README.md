# NUBAN Validator

This project is a collection of implementation of static validators for NUBAN system for account numbers. The implementation is based on the CBN document for NUBAN system, see [this document](https://www.cbn.gov.ng/OUT/2011/CIRCULARS/BSPD/NUBAN PROPOSALS V 0 4- 03 09 2010.PDF)


## Java Implementation

The Java implementation Maven Based. Root for the Java project is `/path/to/project/java`. The following examples are executed from  the Java directory,

### Testing the Project

```
mvn test
```

### Running the Test Application
```
mvn exec:java -Dexec.mainClass="co.onefi.utils.nubanvalidator.App"
```

The output should look like the following:

```
Please enter account number: 0037076291
Specify bank code: 058
Account number is: VALID
Try another Y/N? [N]
```


### API Documentation

`co.onefi.utils.nubanvalidator.NubanValidator.isValid(String bankCode, String accountNumber)` will return a true or false if the account number is valid against the specified bank code. An `IllegalArgumentException` will be thrown if: 

 - Account number or bank code is null.
 - Account number is not 10 digits or Bank code is not 3 digits.
 - Either Account number or Bank code is non-numeric.


## JavaScript / Node.js

The node folder contains code to make this work for a Node.js project.

### Testing

```
npm install && npm test
``` 

### Running in the Broswer

To run in the browser, install Live-server, 

```
npm install -g live-server
```

Then run the following from the node directory:

```
PORT=30080 live-server
```

A browser window should be opened automatically, if not open your browser and navigate to http://localhost:30080


### Browser JS API

See the [index.html](node/index.html) file for example usage. 

- `NubanValidator.isValid(bankCode, accountNubmer)` returns result object : `{status: Boolean, error: String}`, `status` is `true` if the account number is valid against the specified bank code, and `false` otherwise, and the `error` field is populated with the appropriate error message.


### Node.js API

```
var Validator = require('/path/to/project/node');
//{status: Boolean, error: String}
var result = Validator.isValid('231', '0123456789');
```

## Bank Code List

You can find the list of Bank Codes Here:

https://onecredit.atlassian.net/wiki/display/OCL/Bank+List