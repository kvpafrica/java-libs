package co.onefi.utils.nubanvalidator;

/**
 *
 * @author taiwo
 */
public class NubanValidator {

    public static final int BANK_CODE_LENGTH = 3;
    public static final int NUBAN_LENGTH = 10;
    private static final int[] MULTIPLIERS = new int[]{3, 7, 3, 3, 7, 3, 3, 7, 3, 3, 7, 3};

    /**
     * Computes the 10th digit (check digit) of a NUBAN account number given first 9 digits and bank code
     * @param bankCode the 3 digit bank code
     * @param accountNumber first 9 digit of the NUBAN account number
     * @return 
     */
    public static int getCheckDigit(String bankCode, String accountNumber) {
        if (bankCode == null || accountNumber == null) {
            throw new IllegalArgumentException("Account number and bank code must be specified");
        }

        if (!bankCode.matches("^\\d+$") || !accountNumber.matches("^\\d+$")) {
            throw new IllegalArgumentException("Only digits should be specified.");
        }

        if (bankCode.length() != BANK_CODE_LENGTH) {
            throw new IllegalArgumentException("Bank code must be " + BANK_CODE_LENGTH + " digits long.");
        }

        if (accountNumber.length() < NUBAN_LENGTH - 1) {
            throw new IllegalArgumentException("Account number must be at least " + (NUBAN_LENGTH - 1) + " digits long.");
        }

        //Step 1. Calculate A*3+B*7+C*3+D*3+E*7+F*3+G*3+H*7+I*3+J*3+K*7+L*3
        String form = (bankCode + accountNumber);
        int result = 0;
        for (int i = 0; i < MULTIPLIERS.length; i++) {
            result += MULTIPLIERS[i] * Integer.parseInt(String.valueOf(form.charAt(i)));
        }

        //Step 2. Calculate Modulo 10 of your result i.e. the remainder after dividing by 10
        //Step 3. Subtract your result from 10 to get the Check Digit
        //Step 4. If your result is 10, then use 0 as your check digit
        return (10 - result % 10) % 10;
    }
    
    /**
     * 
     * 
     * Validates the NUBAN account number against a given bank code.
     * @param bankCode the 3-digit bank code
     * @param accountNumber the 10-digit NUBAN number
     * @return true if valid and false otherwise
     */
    public static boolean isValid(String bankCode, String accountNumber) {
        if (accountNumber == null || accountNumber.length() != NUBAN_LENGTH) {
            throw new IllegalArgumentException("Account number must be " + NUBAN_LENGTH + " digits long.");
        }

        return getCheckDigit(bankCode, accountNumber) == Integer.parseInt(String.valueOf(accountNumber.charAt(accountNumber.length() - 1)));
    }

}
