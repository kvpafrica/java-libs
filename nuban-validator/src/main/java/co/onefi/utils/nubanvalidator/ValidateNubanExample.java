package co.onefi.utils.nubanvalidator;

import java.util.Scanner;

/**
 *
 * Demonstrates how to validate a NUBAN account number against the bank code.
 * @author taiwo
 */
public class ValidateNubanExample {

    public static void main(String[] args) {
        String another;
        do {
            Scanner sc = new Scanner(System.in);
            System.out.print("Please enter account number: ");
            String account = sc.nextLine().trim();
            System.out.print("Specify bank code: ");
            String bankCode = sc.nextLine().trim();
            try {
                boolean valid = NubanValidator.isValid(bankCode, account);
                System.err.println("Account number is: " + (valid ? "VALID" : "INVALID"));
            } catch (IllegalArgumentException error) {
                System.err.println("An error occured: " + error.getMessage());
            }

            System.out.print("Try another Y/N? [N]");
            another = sc.nextLine().trim();
        } while ("Y".equalsIgnoreCase(another));
    }
}
