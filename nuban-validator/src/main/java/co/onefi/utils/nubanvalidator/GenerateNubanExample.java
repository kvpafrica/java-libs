package co.onefi.utils.nubanvalidator;

import java.util.Scanner;

/**
 *
 * This example demonstrates how to generate a NUBAN from bank code and 9 digit account number
 * 
 * @author taiwo
 * 
 */
public class GenerateNubanExample {

    public static void main(String[] args) {
        String another;
        do {
            Scanner sc = new Scanner(System.in);
            System.out.print("Specify bank code: ");
            String bankCode = sc.nextLine().trim();

            System.out.print("Please enter first 9 digit of account number [Random]: ");
            String account = sc.nextLine().trim();

            try {
                if (account.trim().isEmpty()) {
                    account = randomAccountNumber();
                }
                System.out.println("Input Account number:" + account);
                int digit = NubanValidator.getCheckDigit(bankCode, account);
                System.err.println("Output Account number: " + (account + digit));
            } catch (IllegalArgumentException error) {
                System.err.println("An error occured: " + error.getMessage());
            }

            System.out.print("Try another Y/N? [N]");
            another = sc.nextLine().trim();
        } while ("Y".equalsIgnoreCase(another));
    }

    public static String randomAccountNumber() {
        StringBuilder acct = new StringBuilder();
        for (int i = 0; i < NubanValidator.NUBAN_LENGTH - 1; i++) {
            acct.append((int) Math.floor(Math.random() * 10));
        }

        return acct.toString();
    }
}
