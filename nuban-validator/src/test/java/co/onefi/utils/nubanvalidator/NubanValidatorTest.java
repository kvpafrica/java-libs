/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.onefi.utils.nubanvalidator;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author taiwo
 */
public class NubanValidatorTest {

    public NubanValidatorTest() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testIsValid() {
        //GTB Account with GTB Code
        assertTrue(NubanValidator.isValid("058", "0037076291"));
        //GTB Account with DB Code
        assertFalse(NubanValidator.isValid("063", "0037076291"));
        //DB Account with DB Code
        assertTrue(NubanValidator.isValid("063", "0033336508"));
        //DB Account with GTB Code
        assertFalse(NubanValidator.isValid("058", "0033336508"));
        //Stanbic Account, Stanbic code
        assertTrue(NubanValidator.isValid("221", "0016586078"));
        //Stanbic Account, GTB code
        assertFalse(NubanValidator.isValid("058", "0016586078"));
        //Access Account, with access code
        assertTrue(NubanValidator.isValid("044", "0692791642"));
        //Access Account, with GTB Code
        assertFalse(NubanValidator.isValid("058", "0692791642"));
        //Access Account, with Stanbic Code
        assertFalse(NubanValidator.isValid("221", "0692791642"));
        //Access Account, with DB Code
        assertFalse(NubanValidator.isValid("063", "0692791642"));
    }

    @Test
    public void testIsValid_InvalidParamBankCode() {
        assertThrows(IllegalArgumentException.class, () -> NubanValidator.isValid(null, "0123456789"));
    }

    @Test
    public void testIsValid_InvalidParamNuban() {
        assertThrows(IllegalArgumentException.class, () -> NubanValidator.isValid("012", null));
    }

    @Test
    public void testIsValid_InvalidLengthBankCode() {
        assertThrows(IllegalArgumentException.class, () -> NubanValidator.isValid("12", "0123456789"));
    }

    @Test
    public void testIsValid_InvalidLengthNuban() {
        assertThrows(IllegalArgumentException.class, () -> NubanValidator.isValid("012", "012345678"));
    }

}
