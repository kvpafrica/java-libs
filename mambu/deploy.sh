#!/bin/bash
export APP_DIR=`dirname $0`
cd $APP_DIR
export MAMBU_MODELS_VERSION=$1
export MAMBU_APIS_JAVA_VERSION=$2
export DEPLOY_GIT="git:releases_via_java-libs://git@bitbucket.org:kvpafrica/java-libs.git"

mvn deploy:deploy-file \
        -DgroupId=com.mambu \
        -DartifactId=mambumodels \
        -Dversion=$MAMBU_MODELS_VERSION \
        -Dfile="mambu-apis-java/lib/mambu-models-V${MAMBU_MODELS_VERSION}.jar" \
        -DgeneratePom=true \
        -DrepositoryId=onefi-bb \
        -Durl=$DEPLOY_GIT


mvn deploy:deploy-file \
        -DgroupId=Mambu-APIs-Java \
        -DartifactId=Mambu-APIs-Java \
        -Dversion="${MAMBU_APIS_JAVA_VERSION}-bin" \
        -Dfile="mambu-apis-java/build/Mambu-APIs-Java-${MAMBU_APIS_JAVA_VERSION}-bin.jar" \
        -DpomFile="poms/pom.${MAMBU_APIS_JAVA_VERSION}.xml" \
        -DrepositoryId=onefi-bb \
        -Dsources="mambu-apis-java/build/Mambu-APIs-Java-${MAMBU_APIS_JAVA_VERSION}-bin-sources.jar" \
        -Durl=$DEPLOY_GIT