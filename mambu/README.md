# Mambu SDK Libraries

This contains files for deploying new versions of the Mambu SDK to a more usable Maven Repository. 


## Creating New Versions

The original Mambu Libraries are not usable as-is because they rely on `system` score which causes POM files to break when exported. In order to use Mambu-Apis-Java library with other projects we need to modify the original POM file for the library. To do this, follow the steps below:


**Step 1**

Update the GIT submodule `git submodule update --remote --merge`. See: https://stackoverflow.com/questions/5828324/update-git-submodule-to-latest-commit-on-origin/21195182#21195182

**Step 2**

Extract the POM of the current version you want to update. For the latest version, the `pom.xml` file in the mambu-apis-java directory should suffice. For other versions, unzip the JAR file and copy out the pom.xml file

**Step 3**

Save POM file from above in the poms directory, using the name `pom.{VERSION_NAME}.xml` for example if the current version is 5.0-bin, the POM file will be saved in `poms/pom.5.0.xml`

**Step 4**

Open the saved POM file, look for the following dependency:

```
		<dependency>
			<groupId>com.mambu</groupId>
			<artifactId>mambumodels</artifactId>
			<version>5.0</version> <!-- or current version name -->
			<scope>system</scope>
			<systemPath>${project.basedir}/lib/mambu-models-V5.0.jar
			</systemPath>
			<optional>false</optional>
		</dependency>
```

Remove the system scope and replace and path, so that the dependency becomes:

```
		<dependency>
			<groupId>com.mambu</groupId>
			<artifactId>mambumodels</artifactId>
			<version>5.0</version> <!-- or current version name -->
			<optional>false</optional>
		</dependency>
```

Now, save the POM file.

**Step 5**

With POM file saved, you can now deploy using the `deploy.sh` file. The file deploys both the mambu models and the Mambu-Apis-Java to the repository. 

```
cd mambu
sh ./deploy.sh MAMBU_MODELS_VERSION MAMBU_APIS_JAVA_VERSION  
```

Replace `MAMBU_MODELS_VERSION` and `MAMBU_APIS_JAVA_VERSION` in the above, with the version number of the JAR file for Mambu-Models and Mambu-APIs-Java; respectively. Example:
```
sh ./deploy.sh 9.6 9.8
```

**Step 6**

If `Step 5` completes successfully, necessary commits will be made to the `releases_via_java-libs` branch. Raise a PR to merge it into the `releases` branch.
Once merged, do a `fast-forward merge` from the `releases` branch into the `releases_via_java-libs` and `releases_via_wallet-service` branches.


## Using Mambu API Libraries in Your Project


To include these libraries in your project do the following:


```

<dependencies>
    <dependency>
        <groupId>Mambu-APIs-Java</groupId>
		<artifactId>Mambu-APIs-Java</artifactId>
		<version>5.0-bin</version> <!--- Replace 5.0 with your own version -->
    </dependency>
<dependencies>


<repositories>
	<!-- other repos -->
	<repository>
            <id>onefi-bb</id>
            <name>OneFi Bitbucket</name>
	        <releases>
	                <enabled>true</enabled>
	        </releases>
	        <snapshots>
	                <enabled>false</enabled>
	        </snapshots>
	        <url>https://api.bitbucket.org/2.0/repositories/kvpafrica/java-libs/src/releases</url>
	</repository>
	<!-- other repos -->
</repositories>
```

