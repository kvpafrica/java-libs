/*
 * Copyright (C) One Finance & Investments Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by OneFi Developers <developers@onefi.co>, 2018
 */
package co.onefi.services.ext.nip.soap.models;

/**
 *
 * @author taiwo
 */
public interface IBaseResponse {

    String getReturn();

    void setReturn(String _return);
    
}
