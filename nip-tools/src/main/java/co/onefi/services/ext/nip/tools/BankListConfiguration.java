package co.onefi.services.ext.nip.tools;

import jakarta.annotation.PostConstruct;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@ConfigurationProperties("bankList")
@Data
@Validated
@Component
@RefreshScope
@Slf4j
public class BankListConfiguration {

    public static final String INTERNAL_SORT_CODE = "565";

    @NotNull
    private Map<String, List<BankDetails>> countries;

    private final transient Map<String, BankDetails> flatMap = new HashMap<>();

    @PostConstruct
    void initBanks() {
        log.info("Running bankList config refresh {} {}", countries, this);
        filterInvalids();
        countries.values().forEach((banks) -> {
            banks.forEach((b) -> flatMap.put(b.getSortCode(), b));
        });

        //Note, this adds a static value for internal bank code
        BankDetails onefiDetails = new BankDetails();
        onefiDetails.setName("Carbon Finance");
        onefiDetails.setSortCode(INTERNAL_SORT_CODE);
        flatMap.put(INTERNAL_SORT_CODE, onefiDetails);
    }

    void filterInvalids() {
        Map<String, List<BankDetails>> alternateMap = new HashMap<>();
        countries.forEach((country, bankList) -> {
            alternateMap.put(country, bankList.stream()
                    .filter(bank -> bank != null && StringUtils.isNotEmpty(bank.getSortCode()) && StringUtils.isNotEmpty(bank.getName()))
                    .collect(Collectors.toList()));
        });
        countries = alternateMap;
    }

    public BankDetails getBankDetails(String sortCode) {
        return flatMap.get(sortCode);
    }

    public List<BankDetails> getBanks(String country) {
        return countries.getOrDefault(country.toLowerCase(), new ArrayList<>());
    }

    @Data
    public static class BankDetails {

        @NotEmpty
        private String sortCode;
        @NotEmpty
        private String name;
    }
}
