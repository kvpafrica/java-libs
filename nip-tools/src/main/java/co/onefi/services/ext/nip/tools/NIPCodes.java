/**
 * Copyright (C) One Finance & Investments Ltd - All Rights Reserved
 * Unauthorised copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by OneFi Developers <developers@onefi.co>, May 2018
 */
package co.onefi.services.ext.nip.tools;

import com.google.common.collect.ImmutableSet;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author taiwo
 */
@Slf4j
public class NIPCodes {

    public final static Map<String, String> STATUS_CODES = new FinalizedHashMap<>();
    public final static Map<String, String> REASON_CODES = new FinalizedHashMap<>();
    public final static Map<String, String> BANK_CODES = new FinalizedHashMap<>();

    public static final String STATUS_SUCCESSFUL = "00";
    public static final String STATUS_STATUS_UNKNOWN = "01";
    public static final String STATUS_INVALID_SENDER = "03";
    public static final String STATUS_DO_NOT_HONOR = "05";
    public static final String STATUS_DORMANT_ACCOUNT = "06";
    public static final String STATUS_INVALID_ACCOUNT = "07";
    public static final String STATUS_ACCOUNT_MISMATCH = "08";
    public static final String STATUS_IN_PROGRESS = "09";
    public static final String STATUS_INVALID_TRANSACTION = "12";
    public static final String STATUS_INVALID_AMOUNT = "13";
    public static final String STATUS_INVALID_BATCH_NUMBER = "14";
    public static final String STATUS_INVALID_SESSION_OR_RECORD_ID = "15";
    public static final String STATUS_UNKNOWN_BANK_CODE = "16";
    public static final String STATUS_INVALID_CHANNEL = "17";
    public static final String STATUS_WRONG_METHOD_CALL = "18";
    public static final String STATUS_NO_ACTION_TAKEN = "21";
    public static final String STATUS_UNABLE_TO_LOCATE_RECORD = "25";
    public static final String STATUS_DUPLICATE_RECORD = "26";
    public static final String STATUS_FORMAT_ERROR = "30";
    public static final String STATUS_SUSPECTED_FRAUD = "34";
    public static final String STATUS_CONTACT_SENDING_BANK = "35";
    public static final String STATUS_NO_SUFFICIENT_FUNDS = "51";
    public static final String STATUS_NOT_PERMITTED_TO_SENDER = "57";
    public static final String STATUS_NOT_PERMITTED_ON_CHANNEL = "58";
    public static final String STATUS_TRANSFER_LIMIT_EXCEEDED = "61";
    public static final String STATUS_SECURITY_VIOLATION = "63";
    public static final String STATUS_EXCEEDS_WITHDRAWAL_FREQUENCY = "65";
    public static final String STATUS_RESPONSE_TOO_LATE = "68";
    public static final String STATUS_UNSUCCESSFUL_BLOCK = "69";
    public static final String STATUS_UNSUCCESSFUL_UNBLOCK = "70";
    public static final String STATUS_EMPTY_MANDATE_REFERENCE = "71";
    public static final String STATUS_BANK_NOT_AVAILABLE = "91";
    public static final String STATUS_ROUTING_ERROR = "92";
    public static final String STATUS_DUPLICATE_TRANSACTION = "94";
    public static final String STATUS_SYSTEM_MALFUNCTION = "96";
    public static final String STATUS_TIMEOUT_WAITING_FOR_RESPONSE = "97";

    public static final Set<String> STATUSES_NE_ERROR = ImmutableSet.of(
            "06",
            "07",
            "08",
            "25"
    );

    public static final Set<String> STATUSES_FT_SUCCESS = ImmutableSet.of(
            "00",
            "09"
    );

    static {
        STATUS_CODES.put("00", "Approved or completed successfully");
        STATUS_CODES.put("01", "Status unknown, please wait for settlement report");
        STATUS_CODES.put("03", "Invalid Sender");
        STATUS_CODES.put("05", "Do not honor");
        STATUS_CODES.put("06", "Dormant Account");
        STATUS_CODES.put("07", "Invalid Account");
        STATUS_CODES.put("08", "Account Name Mismatch");
        STATUS_CODES.put("09", "Request processing in progress");
        STATUS_CODES.put("12", "Invalid transaction");
        STATUS_CODES.put("13", "Invalid Amount");
        STATUS_CODES.put("14", "Invalid Batch Number");
        STATUS_CODES.put("15", "Invalid Session or Record ID");
        STATUS_CODES.put("16", "Unknown Bank Code");
        STATUS_CODES.put("17", "Invalid Channel");
        STATUS_CODES.put("18", "Wrong Method Call");
        STATUS_CODES.put("21", "No action taken");
        STATUS_CODES.put("25", "Unable to locate record");
        STATUS_CODES.put("26", "Duplicate record");
        STATUS_CODES.put("30", "Format error");
        STATUS_CODES.put("34", "Suspected fraud");
        STATUS_CODES.put("35", "Contact sending bank");
        STATUS_CODES.put("51", "No sufficient funds");
        STATUS_CODES.put("57", "Transaction not permitted to sender");
        STATUS_CODES.put("58", "Transaction not permitted on channel");
        STATUS_CODES.put("61", "Transfer limit Exceeded");
        STATUS_CODES.put("63", "Security violation");
        STATUS_CODES.put("65", "Exceeds withdrawal frequency");
        STATUS_CODES.put("68", "Response received too late");
        STATUS_CODES.put("69", "Unsuccessful Account/Amount block");
        STATUS_CODES.put("70", "Unsuccessful Account/Amount unblock");
        STATUS_CODES.put("71", "Empty Mandate Reference Number");
        STATUS_CODES.put("91", "Beneficiary Bank not available");
        STATUS_CODES.put("92", "Routing error");
        STATUS_CODES.put("94", "Duplicate transaction");
        STATUS_CODES.put("96", "System malfunction");
        STATUS_CODES.put("97", "Timeout waiting for response from destination");
    }

    static {
        REASON_CODES.put("0001", "Suspected fraud");
        REASON_CODES.put("0002", "Security violation");
        REASON_CODES.put("0003", "Multiple cases of insufficient fund");
        REASON_CODES.put("0004", "Multiple cases of “Transfer limit Exceeded”");
        REASON_CODES.put("0005", "Non-compliance with operating regulations");
        REASON_CODES.put("0006", "Identity theft");
        REASON_CODES.put("0007", "Duplicate transaction processing");
        REASON_CODES.put("0008", "Fraudulent multiple transactions");
        REASON_CODES.put("0009", "Payment made by other means");
        REASON_CODES.put("0010", "Purpose of payment not redeemed");
        REASON_CODES.put("0011", "Recurring transactions");
        REASON_CODES.put("1111", "Others");
    }

    static {
        //region Old NIP Institution Codes that may have been discontinued (i.e. they may be outdated).
        BANK_CODES.put("084", "000019");
        BANK_CODES.put("303", "100015");
        BANK_CODES.put("307", "100030");
        BANK_CODES.put("502", "090004");
        //endregion

        //region NIP Institution Codes as at 2023-01-19.
        BANK_CODES.put("232", "000001");
        BANK_CODES.put("082", "000002");
        BANK_CODES.put("214", "000003");
        BANK_CODES.put("033", "000004");
        BANK_CODES.put("063", "000005");
        BANK_CODES.put("301", "000006");
        BANK_CODES.put("070", "000007");
        BANK_CODES.put("076", "000008");
        BANK_CODES.put("023", "000009");
        BANK_CODES.put("050", "000010");
        BANK_CODES.put("215", "000011");
        BANK_CODES.put("221", "000012");
        BANK_CODES.put("058", "000013");
        BANK_CODES.put("044", "000014");
        BANK_CODES.put("057", "000015");
        BANK_CODES.put("011", "000016");
        BANK_CODES.put("035", "000017");
        BANK_CODES.put("032", "000018");
        BANK_CODES.put("030", "000020");
        BANK_CODES.put("068", "000021");
        BANK_CODES.put("100", "000022");
        BANK_CODES.put("101", "000023");
        BANK_CODES.put("000024", "000024");
        BANK_CODES.put("000025", "000025");
        BANK_CODES.put("000026", "000026");
        BANK_CODES.put("000027", "000027");
        BANK_CODES.put("000028", "000028");
        BANK_CODES.put("000029", "000029");
        BANK_CODES.put("000030", "000030");
        BANK_CODES.put("000031", "000031");
        BANK_CODES.put("000033", "000033");
        BANK_CODES.put("000034", "000034");
        BANK_CODES.put("000036", "000036");
        BANK_CODES.put("050001", "050001");
        BANK_CODES.put("050002", "050002");
        BANK_CODES.put("050003", "050003");
        BANK_CODES.put("050004", "050004");
        BANK_CODES.put("050005", "050005");
        BANK_CODES.put("050006", "050006");
        BANK_CODES.put("050007", "050007");
        BANK_CODES.put("050009", "050009");
        BANK_CODES.put("050010", "050010");
        BANK_CODES.put("559", "060001");
        BANK_CODES.put("060002", "060002");
        BANK_CODES.put("060003", "060003");
        BANK_CODES.put("060004", "060004");
        BANK_CODES.put("552", "070001");
        BANK_CODES.put("501", "070002");
        BANK_CODES.put("551", "070006");
        BANK_CODES.put("606", "070007");
        BANK_CODES.put("560", "070008");
        BANK_CODES.put("070009", "070009");
        BANK_CODES.put("070010", "070010");
        BANK_CODES.put("070011", "070011");
        BANK_CODES.put("070012", "070012");
        BANK_CODES.put("070013", "070013");
        BANK_CODES.put("070014", "070014");
        BANK_CODES.put("070015", "070015");
        BANK_CODES.put("070016", "070016");
        BANK_CODES.put("070017", "070017");
        BANK_CODES.put("070019", "070019");
        BANK_CODES.put("070021", "070021");
        BANK_CODES.put("070022", "070022");
        BANK_CODES.put("070023", "070023");
        BANK_CODES.put("070024", "070024");
        BANK_CODES.put("070025", "070025");
        BANK_CODES.put("070026", "070026");
        BANK_CODES.put("080002", "080002");
        BANK_CODES.put("401", "090001");
        BANK_CODES.put("402", "090003");
        BANK_CODES.put("523", "090005");
        BANK_CODES.put("403", "090006");
        BANK_CODES.put("562", "090097");
        BANK_CODES.put("413", "090107");
        BANK_CODES.put("561", "090108");
        BANK_CODES.put("090110", "090110"); // VFD: Sort Code on CIPS is "566".
        BANK_CODES.put("608", "090111");
        BANK_CODES.put("609", "090112");
        BANK_CODES.put("090113", "090113");
        BANK_CODES.put("090114", "090114");
        BANK_CODES.put("090115", "090115");
        BANK_CODES.put("090116", "090116");
        BANK_CODES.put("090117", "090117");
        BANK_CODES.put("090118", "090118");
        BANK_CODES.put("090119", "090119");
        BANK_CODES.put("090120", "090120");
        BANK_CODES.put("090121", "090121");
        BANK_CODES.put("090122", "090122");
        BANK_CODES.put("090123", "090123");
        BANK_CODES.put("090124", "090124");
        BANK_CODES.put("090125", "090125");
        BANK_CODES.put("090126", "090126");
        BANK_CODES.put("090127", "090127");
        BANK_CODES.put("090128", "090128");
        BANK_CODES.put("090129", "090129");
        BANK_CODES.put("090130", "090130");
        BANK_CODES.put("090131", "090131");
        BANK_CODES.put("090132", "090132");
        BANK_CODES.put("090133", "090133");
        BANK_CODES.put("090134", "090134");
        BANK_CODES.put("090135", "090135");
        BANK_CODES.put("090136", "090136");
        BANK_CODES.put("090137", "090137");
        BANK_CODES.put("090138", "090138");
        BANK_CODES.put("090139", "090139");
        BANK_CODES.put("090140", "090140");
        BANK_CODES.put("090141", "090141");
        BANK_CODES.put("090142", "090142");
        BANK_CODES.put("090143", "090143");
        BANK_CODES.put("090144", "090144");
        BANK_CODES.put("090145", "090145");
        BANK_CODES.put("090146", "090146");
        BANK_CODES.put("090147", "090147");
        BANK_CODES.put("090148", "090148");
        BANK_CODES.put("090149", "090149");
        BANK_CODES.put("090150", "090150");
        BANK_CODES.put("090151", "090151");
        BANK_CODES.put("090152", "090152");
        BANK_CODES.put("090153", "090153");
        BANK_CODES.put("090154", "090154");
        BANK_CODES.put("090155", "090155");
        BANK_CODES.put("090156", "090156");
        BANK_CODES.put("090157", "090157");
        BANK_CODES.put("090158", "090158");
        BANK_CODES.put("090159", "090159");
        BANK_CODES.put("090160", "090160");
        BANK_CODES.put("090161", "090161");
        BANK_CODES.put("090162", "090162");
        BANK_CODES.put("090163", "090163");
        BANK_CODES.put("090164", "090164");
        BANK_CODES.put("090165", "090165");
        BANK_CODES.put("090166", "090166");
        BANK_CODES.put("090167", "090167");
        BANK_CODES.put("090168", "090168");
        BANK_CODES.put("090169", "090169");
        BANK_CODES.put("090170", "090170");
        BANK_CODES.put("090171", "090171");
        BANK_CODES.put("090172", "090172");
        BANK_CODES.put("090173", "090173");
        BANK_CODES.put("090174", "090174");
        BANK_CODES.put("090175", "090175");
        BANK_CODES.put("090176", "090176");
        BANK_CODES.put("090177", "090177");
        BANK_CODES.put("090178", "090178");
        BANK_CODES.put("090179", "090179");
        BANK_CODES.put("090180", "090180");
        BANK_CODES.put("090181", "090181");
        BANK_CODES.put("090182", "090182");
        BANK_CODES.put("090186", "090186");
        BANK_CODES.put("090188", "090188");
        BANK_CODES.put("090189", "090189");
        BANK_CODES.put("090190", "090190");
        BANK_CODES.put("090191", "090191");
        BANK_CODES.put("090192", "090192");
        BANK_CODES.put("090193", "090193");
        BANK_CODES.put("090194", "090194");
        BANK_CODES.put("090195", "090195");
        BANK_CODES.put("090196", "090196");
        BANK_CODES.put("090197", "090197");
        BANK_CODES.put("090198", "090198");
        BANK_CODES.put("090201", "090201");
        BANK_CODES.put("090202", "090202");
        BANK_CODES.put("090205", "090205");
        BANK_CODES.put("090211", "090211");
        BANK_CODES.put("090251", "090251");
        BANK_CODES.put("090252", "090252");
        BANK_CODES.put("090254", "090254");
        BANK_CODES.put("090258", "090258");
        BANK_CODES.put("090259", "090259");
        BANK_CODES.put("090260", "090260");
        BANK_CODES.put("090261", "090261");
        BANK_CODES.put("090262", "090262");
        BANK_CODES.put("090263", "090263");
        BANK_CODES.put("090264", "090264");
        BANK_CODES.put("090265", "090265");
        BANK_CODES.put("090266", "090266");
        BANK_CODES.put("090267", "090267");
        BANK_CODES.put("090268", "090268");
        BANK_CODES.put("090269", "090269");
        BANK_CODES.put("090270", "090270");
        BANK_CODES.put("090271", "090271");
        BANK_CODES.put("090272", "090272");
        BANK_CODES.put("090273", "090273");
        BANK_CODES.put("090274", "090274");
        BANK_CODES.put("090275", "090275");
        BANK_CODES.put("090276", "090276");
        BANK_CODES.put("090277", "090277");
        BANK_CODES.put("090278", "090278");
        BANK_CODES.put("090279", "090279");
        BANK_CODES.put("090280", "090280");
        BANK_CODES.put("090281", "090281");
        BANK_CODES.put("090282", "090282");
        BANK_CODES.put("090283", "090283");
        BANK_CODES.put("090285", "090285");
        BANK_CODES.put("090286", "090286");
        BANK_CODES.put("090287", "090287");
        BANK_CODES.put("090289", "090289");
        BANK_CODES.put("090290", "090290");
        BANK_CODES.put("090291", "090291");
        BANK_CODES.put("090292", "090292");
        BANK_CODES.put("090293", "090293");
        BANK_CODES.put("090294", "090294");
        BANK_CODES.put("090295", "090295");
        BANK_CODES.put("090296", "090296");
        BANK_CODES.put("090297", "090297");
        BANK_CODES.put("090298", "090298");
        BANK_CODES.put("090299", "090299");
        BANK_CODES.put("090302", "090302");
        BANK_CODES.put("090303", "090303");
        BANK_CODES.put("090304", "090304");
        BANK_CODES.put("090305", "090305");
        BANK_CODES.put("090307", "090307");
        BANK_CODES.put("090308", "090308");
        BANK_CODES.put("090310", "090310");
        BANK_CODES.put("090315", "090315");
        BANK_CODES.put("090316", "090316");
        BANK_CODES.put("090317", "090317");
        BANK_CODES.put("090318", "090318");
        BANK_CODES.put("090319", "090319");
        BANK_CODES.put("090320", "090320");
        BANK_CODES.put("090321", "090321");
        BANK_CODES.put("090322", "090322");
        BANK_CODES.put("090323", "090323");
        BANK_CODES.put("090324", "090324");
        BANK_CODES.put("090325", "090325");
        BANK_CODES.put("090326", "090326");
        BANK_CODES.put("090327", "090327");
        BANK_CODES.put("090328", "090328");
        BANK_CODES.put("090329", "090329");
        BANK_CODES.put("090330", "090330");
        BANK_CODES.put("090331", "090331");
        BANK_CODES.put("090332", "090332");
        BANK_CODES.put("090333", "090333");
        BANK_CODES.put("090335", "090335");
        BANK_CODES.put("090336", "090336");
        BANK_CODES.put("090337", "090337");
        BANK_CODES.put("090338", "090338");
        BANK_CODES.put("090340", "090340");
        BANK_CODES.put("090341", "090341");
        BANK_CODES.put("090343", "090343");
        BANK_CODES.put("090345", "090345");
        BANK_CODES.put("090349", "090349");
        BANK_CODES.put("090350", "090350");
        BANK_CODES.put("090352", "090352");
        BANK_CODES.put("090353", "090353");
        BANK_CODES.put("090360", "090360");
        BANK_CODES.put("090362", "090362");
        BANK_CODES.put("090363", "090363");
        BANK_CODES.put("090364", "090364");
        BANK_CODES.put("090365", "090365");
        BANK_CODES.put("090366", "090366");
        BANK_CODES.put("090369", "090369");
        BANK_CODES.put("090370", "090370");
        BANK_CODES.put("090371", "090371");
        BANK_CODES.put("090372", "090372");
        BANK_CODES.put("090373", "090373");
        BANK_CODES.put("090374", "090374");
        BANK_CODES.put("090376", "090376");
        BANK_CODES.put("090377", "090377");
        BANK_CODES.put("090378", "090378");
        BANK_CODES.put("090379", "090379");
        BANK_CODES.put("090380", "090380");
        BANK_CODES.put("090383", "090383");
        BANK_CODES.put("090385", "090385");
        BANK_CODES.put("090386", "090386");
        BANK_CODES.put("090389", "090389");
        BANK_CODES.put("090390", "090390");
        BANK_CODES.put("090391", "090391");
        BANK_CODES.put("090392", "090392");
        BANK_CODES.put("090393", "090393");
        BANK_CODES.put("090394", "090394");
        BANK_CODES.put("090395", "090395");
        BANK_CODES.put("090396", "090396");
        BANK_CODES.put("090397", "090397");
        BANK_CODES.put("090398", "090398");
        BANK_CODES.put("090399", "090399");
        BANK_CODES.put("090400", "090400");
        BANK_CODES.put("090401", "090401");
        BANK_CODES.put("090402", "090402");
        BANK_CODES.put("090403", "090403");
        BANK_CODES.put("090404", "090404");
        BANK_CODES.put("090405", "090405");
        BANK_CODES.put("090406", "090406");
        BANK_CODES.put("090408", "090408");
        BANK_CODES.put("090409", "090409");
        BANK_CODES.put("090410", "090410");
        BANK_CODES.put("090411", "090411");
        BANK_CODES.put("090412", "090412");
        BANK_CODES.put("090413", "090413");
        BANK_CODES.put("090414", "090414");
        BANK_CODES.put("090415", "090415");
        BANK_CODES.put("090416", "090416");
        BANK_CODES.put("090417", "090417");
        BANK_CODES.put("090418", "090418");
        BANK_CODES.put("090419", "090419");
        BANK_CODES.put("090420", "090420");
        BANK_CODES.put("090421", "090421");
        BANK_CODES.put("090422", "090422");
        BANK_CODES.put("090423", "090423");
        BANK_CODES.put("090424", "090424");
        BANK_CODES.put("090425", "090425");
        BANK_CODES.put("090426", "090426");
        BANK_CODES.put("090427", "090427");
        BANK_CODES.put("090428", "090428");
        BANK_CODES.put("090429", "090429");
        BANK_CODES.put("090430", "090430");
        BANK_CODES.put("090431", "090431");
        BANK_CODES.put("090432", "090432");
        BANK_CODES.put("090433", "090433");
        BANK_CODES.put("090434", "090434");
        BANK_CODES.put("090435", "090435");
        BANK_CODES.put("090436", "090436");
        BANK_CODES.put("090437", "090437");
        BANK_CODES.put("090438", "090438");
        BANK_CODES.put("090439", "090439");
        BANK_CODES.put("090440", "090440");
        BANK_CODES.put("090441", "090441");
        BANK_CODES.put("090443", "090443");
        BANK_CODES.put("090444", "090444");
        BANK_CODES.put("090445", "090445");
        BANK_CODES.put("090446", "090446");
        BANK_CODES.put("090448", "090448");
        BANK_CODES.put("090449", "090449");
        BANK_CODES.put("090450", "090450");
        BANK_CODES.put("090451", "090451");
        BANK_CODES.put("090452", "090452");
        BANK_CODES.put("090453", "090453");
        BANK_CODES.put("090454", "090454");
        BANK_CODES.put("090455", "090455");
        BANK_CODES.put("090456", "090456");
        BANK_CODES.put("090459", "090459");
        BANK_CODES.put("090460", "090460");
        BANK_CODES.put("090461", "090461");
        BANK_CODES.put("090462", "090462");
        BANK_CODES.put("090463", "090463");
        BANK_CODES.put("090464", "090464");
        BANK_CODES.put("090465", "090465");
        BANK_CODES.put("090466", "090466");
        BANK_CODES.put("090467", "090467");
        BANK_CODES.put("090468", "090468");
        BANK_CODES.put("090469", "090469");
        BANK_CODES.put("090470", "090470");
        BANK_CODES.put("090471", "090471");
        BANK_CODES.put("090472", "090472");
        BANK_CODES.put("090473", "090473");
        BANK_CODES.put("090474", "090474");
        BANK_CODES.put("090475", "090475");
        BANK_CODES.put("090476", "090476");
        BANK_CODES.put("090477", "090477");
        BANK_CODES.put("090478", "090478");
        BANK_CODES.put("090479", "090479");
        BANK_CODES.put("090480", "090480");
        BANK_CODES.put("090481", "090481");
        BANK_CODES.put("090482", "090482");
        BANK_CODES.put("090483", "090483");
        BANK_CODES.put("090484", "090484");
        BANK_CODES.put("090485", "090485");
        BANK_CODES.put("090486", "090486");
        BANK_CODES.put("090487", "090487");
        BANK_CODES.put("090488", "090488");
        BANK_CODES.put("090489", "090489");
        BANK_CODES.put("090490", "090490");
        BANK_CODES.put("090491", "090491");
        BANK_CODES.put("090492", "090492");
        BANK_CODES.put("090493", "090493");
        BANK_CODES.put("090494", "090494");
        BANK_CODES.put("090495", "090495");
        BANK_CODES.put("090496", "090496");
        BANK_CODES.put("090497", "090497");
        BANK_CODES.put("090498", "090498");
        BANK_CODES.put("090499", "090499");
        BANK_CODES.put("090500", "090500");
        BANK_CODES.put("090501", "090501");
        BANK_CODES.put("090502", "090502");
        BANK_CODES.put("090503", "090503");
        BANK_CODES.put("090504", "090504");
        BANK_CODES.put("090505", "090505");
        BANK_CODES.put("090506", "090506");
        BANK_CODES.put("090507", "090507");
        BANK_CODES.put("090508", "090508");
        BANK_CODES.put("090509", "090509");
        BANK_CODES.put("090510", "090510");
        BANK_CODES.put("090511", "090511");
        BANK_CODES.put("090512", "090512");
        BANK_CODES.put("090513", "090513");
        BANK_CODES.put("090514", "090514");
        BANK_CODES.put("090515", "090515");
        BANK_CODES.put("090516", "090516");
        BANK_CODES.put("090517", "090517");
        BANK_CODES.put("090518", "090518");
        BANK_CODES.put("090519", "090519");
        BANK_CODES.put("090520", "090520");
        BANK_CODES.put("090521", "090521");
        BANK_CODES.put("090523", "090523");
        BANK_CODES.put("090524", "090524");
        BANK_CODES.put("090525", "090525");
        BANK_CODES.put("090526", "090526");
        BANK_CODES.put("090527", "090527");
        BANK_CODES.put("090528", "090528");
        BANK_CODES.put("090529", "090529");
        BANK_CODES.put("090530", "090530");
        BANK_CODES.put("090531", "090531");
        BANK_CODES.put("090532", "090532");
        BANK_CODES.put("090534", "090534");
        BANK_CODES.put("090535", "090535");
        BANK_CODES.put("090536", "090536");
        BANK_CODES.put("090537", "090537");
        BANK_CODES.put("090538", "090538");
        BANK_CODES.put("090539", "090539");
        BANK_CODES.put("090540", "090540");
        BANK_CODES.put("090541", "090541");
        BANK_CODES.put("090543", "090543");
        BANK_CODES.put("090544", "090544");
        BANK_CODES.put("090545", "090545");
        BANK_CODES.put("090546", "090546");
        BANK_CODES.put("090547", "090547");
        BANK_CODES.put("090548", "090548");
        BANK_CODES.put("090549", "090549");
        BANK_CODES.put("090550", "090550");
        BANK_CODES.put("090551", "090551");
        BANK_CODES.put("090552", "090552");
        BANK_CODES.put("090553", "090553");
        BANK_CODES.put("090554", "090554");
        BANK_CODES.put("090555", "090555");
        BANK_CODES.put("090556", "090556");
        BANK_CODES.put("090557", "090557");
        BANK_CODES.put("090558", "090558");
        BANK_CODES.put("090559", "090559");
        BANK_CODES.put("090560", "090560");
        BANK_CODES.put("090561", "090561");
        BANK_CODES.put("090562", "090562");
        BANK_CODES.put("090563", "090563");
        BANK_CODES.put("090564", "090564");
        BANK_CODES.put("090565", "090565");
        BANK_CODES.put("090566", "090566");
        BANK_CODES.put("090567", "090567");
        BANK_CODES.put("090568", "090568");
        BANK_CODES.put("090569", "090569");
        BANK_CODES.put("090570", "090570");
        BANK_CODES.put("090571", "090571");
        BANK_CODES.put("090572", "090572");
        BANK_CODES.put("090573", "090573");
        BANK_CODES.put("090574", "090574");
        BANK_CODES.put("090575", "090575");
        BANK_CODES.put("090576", "090576");
        BANK_CODES.put("090578", "090578");
        BANK_CODES.put("090579", "090579");
        BANK_CODES.put("090580", "090580");
        BANK_CODES.put("090581", "090581");
        BANK_CODES.put("090583", "090583");
        BANK_CODES.put("090584", "090584");
        BANK_CODES.put("090586", "090586");
        BANK_CODES.put("090587", "090587");
        BANK_CODES.put("090588", "090588");
        BANK_CODES.put("090589", "090589");
        BANK_CODES.put("090590", "090590");
        BANK_CODES.put("090591", "090591");
        BANK_CODES.put("090592", "090592");
        BANK_CODES.put("090593", "090593");
        BANK_CODES.put("090598", "090598");
        BANK_CODES.put("090599", "090599");
        BANK_CODES.put("090600", "090600");
        BANK_CODES.put("090602", "090602");
        BANK_CODES.put("090603", "090603");
        BANK_CODES.put("314", "100001");
        BANK_CODES.put("327", "100002");
        BANK_CODES.put("311", "100003");
        BANK_CODES.put("305", "100004");    // OPAY: "305" Sort Code is from Remote Config. Kept for backwards compatibility.
        BANK_CODES.put("304", "100004");    // OPAY: "304" Sort Code is from CIPS database.
        BANK_CODES.put("317", "100005");
        BANK_CODES.put("306", "100006");
        BANK_CODES.put("100007", "100007");
        BANK_CODES.put("100008", "100008");
        BANK_CODES.put("315", "100009");
        BANK_CODES.put("319", "100010");
        BANK_CODES.put("100011", "100011"); // M-Kudi: Sort Code on CIPS is "313".
        BANK_CODES.put("320", "100012");
        BANK_CODES.put("323", "100013");
        BANK_CODES.put("309", "100014");
        BANK_CODES.put("100016", "100016");
        BANK_CODES.put("324", "100017");
        BANK_CODES.put("322", "100018");
        BANK_CODES.put("318", "100019");
        BANK_CODES.put("325", "100020");
        BANK_CODES.put("302", "100021");
        BANK_CODES.put("100022", "100022");
        BANK_CODES.put("100023", "100023");
        BANK_CODES.put("415", "100024");
        BANK_CODES.put("100025", "100025");
        BANK_CODES.put("565", "100026");
        BANK_CODES.put("330", "100027");
        BANK_CODES.put("100028", "100028");
        BANK_CODES.put("100029", "100029");
        BANK_CODES.put("100031", "100031");
        BANK_CODES.put("100032", "100032");
        BANK_CODES.put("100033", "100033");
        BANK_CODES.put("100034", "100034");
        BANK_CODES.put("100035", "100035");
        BANK_CODES.put("100036", "100036");
        BANK_CODES.put("100039", "100039");
        BANK_CODES.put("100052", "100052");
        BANK_CODES.put("329", "110001");
        BANK_CODES.put("110002", "110002");
        BANK_CODES.put("110003", "110003");
        BANK_CODES.put("110004", "110004");
        BANK_CODES.put("110005", "110005");
        BANK_CODES.put("110006", "110006");
        BANK_CODES.put("110007", "110007");
        BANK_CODES.put("110008", "110008");
        BANK_CODES.put("110009", "110009");
        BANK_CODES.put("110010", "110010");
        BANK_CODES.put("110011", "110011");
        BANK_CODES.put("110013", "110013");
        BANK_CODES.put("110014", "110014");
        BANK_CODES.put("110015", "110015");
        BANK_CODES.put("110017", "110017");
        BANK_CODES.put("110018", "110018");
        BANK_CODES.put("110019", "110019");
        BANK_CODES.put("110021", "110021");
        BANK_CODES.put("110022", "110022");
        BANK_CODES.put("110023", "110023");
        BANK_CODES.put("110024", "110024");
        BANK_CODES.put("110025", "110025");
        BANK_CODES.put("110026", "110026");
        BANK_CODES.put("110027", "110027");
        BANK_CODES.put("110028", "110028");
        BANK_CODES.put("110029", "110029");
        BANK_CODES.put("120001", "120001");
        BANK_CODES.put("120002", "120002");
        BANK_CODES.put("120003", "120003");
        BANK_CODES.put("120004", "120004");
        BANK_CODES.put("120005", "120005");
        BANK_CODES.put("601", "400001");
        BANK_CODES.put("999001", "999001");
        //endregion
    }

    private static BankListConfiguration bankListConfiguration;

    public static void setBankListConfiguration(BankListConfiguration bankListConfiguration) {
        NIPCodes.bankListConfiguration = bankListConfiguration;
    }

    private NIPCodes() {
    }

    static class FinalizedHashMap<K, V> extends HashMap<K, V> {

        @Override
        public void clear() {
            throw new UnsupportedOperationException("You cannot clear this map.");
        }

        @Override
        public V remove(Object key) {
            throw new UnsupportedOperationException("You cannot remove from this map.");
        }

    }

    public static boolean isSuccess(String code) {
        return STATUS_SUCCESSFUL.equals(code);
    }

    public static String getStatusMessage(String code) {
        return STATUS_CODES.get(code);
    }

    public static String getBankCode(String sortCode, boolean isLive) {
        if (isLive) {
            String code = BANK_CODES.get(sortCode);
            if (code == null) {
                if (NIPCodes.bankListConfiguration != null) {
                    // Try fetching from Remote Config
                    BankListConfiguration.BankDetails bankDetails = NIPCodes.bankListConfiguration.getBankDetails(sortCode);
                    if (bankDetails == null || StringUtils.isEmpty(bankDetails.getSortCode())) {
                        throw new IllegalArgumentException("Sort code: " + sortCode + " is not mapped, and no match was found on remote config.");
                    }
                    code = bankDetails.getSortCode();
                    log.debug("Retrieved Bank Sort Code [{}] from Remote Config for [{}].", code, sortCode);
                } else {
                    log.debug("'NIPCodes.bankListConfiguration' is yet to be initialized!.");
                    throw new IllegalArgumentException("Sort code: " + sortCode + " is not mapped.");
                }
            }

            return code;
        }

        if (sortCode.equals("565")) {
            return "999061";
        }

        return "999" + sortCode.substring(sortCode.length() - 3, sortCode.length());
    }

}
