/**
 * Copyright (C) One Finance & Investments Ltd - All Rights Reserved
 * Unauthorised copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by OneFi Developers <developers@onefi.co>, May 2018
 */
package co.onefi.services.ext.nip.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import nfp.ssm.core.SSMLib;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author taiwo
 */
public class SSMUtil {

    private final String password;

    private final SSMLib lib;

    public SSMUtil(String publicKeyPath, String privateKeyPath, String password) throws IOException {
        this.password = password;
        lib = new SSMLib(resolvePath(publicKeyPath), resolvePath(privateKeyPath));
    }

    private String resolvePath(String path) throws IOException {
        if (new File(path).exists()) {
            return path;
        }

        InputStream is = getClass().getClassLoader().getResourceAsStream(path);
        if (is == null) {
            throw new FileNotFoundException("Could not load the SSM keys");
        }

        File oFile = File.createTempFile(path.replaceAll("[^a-zA-Z0-9]+", "_"), ".tmp");
        oFile.deleteOnExit();

        IOUtils.copy(is, new FileOutputStream(oFile));
        System.out.println("Output Path for file: " + oFile.getAbsolutePath());
        return oFile.getAbsolutePath();
    }

    public String encryptText(String input) {
        return lib.encryptMessage(input);
    }

    public String decryptText(String input) {
        return lib.decryptFile(input, password);

    }
}
