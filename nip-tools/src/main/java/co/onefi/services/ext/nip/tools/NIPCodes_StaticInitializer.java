package co.onefi.services.ext.nip.tools;

import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class NIPCodes_StaticInitializer {
    @Autowired
    private BankListConfiguration bankListConfiguration;

    @Autowired
    private ApplicationContext context;

    @PostConstruct
    public void init() {
        NIPCodes.setBankListConfiguration(bankListConfiguration);
    }
}
