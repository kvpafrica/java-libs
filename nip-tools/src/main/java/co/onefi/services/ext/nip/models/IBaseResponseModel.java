/*
 * Copyright (C) One Finance & Investments Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by OneFi Developers <developers@onefi.co>, 2018
 */
package co.onefi.services.ext.nip.models;

/**
 *
 * @author taiwo
 */
public interface IBaseResponseModel extends ISessionIDObject {

}
