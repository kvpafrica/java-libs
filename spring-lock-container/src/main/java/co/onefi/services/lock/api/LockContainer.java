package co.onefi.services.lock.api;

import java.util.concurrent.locks.Lock;

/**
 *
 * @author taiwo
 */
public interface LockContainer {

    /**
     * Gets the lock with the specified name and expires after the specified mills
     * Any request for acquisition after this lock has expired will lead to a new lock being created, 
     * Hence loss of the initial lock. Or rendering the initial lock irrelevant for the given label.
     *
     * @param name the identifier for this lock
     * @param ttl the maximum time to keep this lock.
     *  
     * @return Lock
     * 
     */
    Lock getLock(String name, long ttl) throws LockAcquisitionException;

    /**
     * Checks if the lock with the specified name exists.
     * @param name
     * @return 
     */
    boolean hasLock(String name);
}
