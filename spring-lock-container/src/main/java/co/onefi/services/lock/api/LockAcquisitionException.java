package co.onefi.services.lock.api;


/**
 *
 * @author taiwo
 */
public class LockAcquisitionException extends RuntimeException {

    public LockAcquisitionException() {
        super();
    }

    public LockAcquisitionException(String message) {
        super(message);
    }

    public LockAcquisitionException(Throwable cause) {
        super(cause);
    }

    public LockAcquisitionException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
