package co.onefi.services.lock.impl.redis;

import co.onefi.services.lock.api.TimedDelegatedLock;
import java.util.concurrent.TimeUnit;

import org.redisson.RedissonFairLock;
import org.redisson.api.RLock;

/**
 *
 * @author taiwo
 */
public class RedissonLockDelegate extends TimedDelegatedLock {

    public RedissonLockDelegate(RLock lock, long timeout) {
        super(lock, timeout);
    }

    @Override
    protected void updateExpiry() {
        ((RedissonFairLock) lock).expire(timeout, TimeUnit.MILLISECONDS);
    }

}
