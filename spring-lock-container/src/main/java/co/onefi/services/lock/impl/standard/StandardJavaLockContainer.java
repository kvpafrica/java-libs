package co.onefi.services.lock.impl.standard;

import co.onefi.services.lock.api.LockContainer;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author taiwo
 */
@Slf4j
public class StandardJavaLockContainer implements LockContainer {

    private final Map<String, TimedLock> locks;

    private static final long DEFAULT_PURGE_INTERVAL = 900000L;

    private static final int DEFAULT_SIZE = 10;
    //purge every 15 minutes
    private long purgeInterval = DEFAULT_PURGE_INTERVAL;

    private long lastPurge;

    private class TimedLock implements Lock {

        long expiryTime = -1;
        final long timeout;
        final Map<Long, ReentrantLock> threadLocks = new ConcurrentHashMap<>();
        final String name;
        ReentrantLock currentLock;
        private final Object LOCK = new Object();

        public TimedLock(long timeout, String name) {
            this.timeout = timeout;
            this.name = name;
        }

        boolean isExpired() {
            return expiryTime <= System.currentTimeMillis();
        }

        private synchronized void updateExpiry() {
            log.info(name + " :: Updating expiry");
            this.expiryTime = System.currentTimeMillis() + timeout;
            log.info(name + " :: Updated expiry");
        }

        private synchronized Lock getValidLock() {
            long threadId = Thread.currentThread().getId();

            if (currentLock == null) {
                log.info(name + " :: No current lock exists, creating new");
                currentLock = new ReentrantLock(true);
            } else if (threadLocks.containsKey(threadId)) {
                ReentrantLock threadLock = threadLocks.get(threadId);
                //check if lock is expired
                log.info(name + " :: thread has a lock, checking validity");
                if (isExpired() && threadLock.isLocked()) {
                    threadLock = threadLock.isHeldByCurrentThread() ? threadLock : new ReentrantLock(true);
                }
                currentLock = threadLock;
            } else {
                log.info(name + " :: current lock exists checking if expired");
                currentLock = isExpired() ? new ReentrantLock(true) : currentLock;
            }
            
            threadLocks.put(threadId, currentLock);
            return currentLock;
        }

        @Override
        public void lockInterruptibly() throws InterruptedException {
            synchronized (LOCK) {
                getValidLock().lockInterruptibly();
                updateExpiry();
            }
        }

        @Override
        public boolean tryLock() {
            synchronized (LOCK) {
                if (getValidLock().tryLock()) {
                    updateExpiry();
                    return true;
                }
            }

            return false;
        }

        @Override
        public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
            synchronized (LOCK) {
                if (getValidLock().tryLock(time, unit)) {
                    updateExpiry();
                    return true;
                }
            }

            return false;
        }

        @Override
        public void unlock() {
            getValidLock().unlock();
        }

        @Override
        public Condition newCondition() {
            return getValidLock().newCondition();
        }

        @Override
        public void lock() {
            synchronized (LOCK) {
                getValidLock().lock();
                updateExpiry();
            }
        }

    }

    public StandardJavaLockContainer(int initialSize) {
        this.locks = new ConcurrentHashMap<>(initialSize);
        lastPurge = System.currentTimeMillis();
    }

    public StandardJavaLockContainer() {
        this(DEFAULT_SIZE);
    }

    @Override
    public synchronized Lock getLock(String name, long ttl) {
        try {
            TimedLock existingLock = locks.get(name);
            if (existingLock == null) {
                //create new lock
                log.debug("Creating new lock for " + name);
                existingLock = new TimedLock(ttl, name);
                locks.putIfAbsent(name, existingLock);
            } else {
                log.warn("Lock still valid... " + name);
            }

            return existingLock;
        } finally {
            long nextPurge = lastPurge + purgeInterval;
            if (System.currentTimeMillis() >= nextPurge) {
                purge();
            }
        }
    }

    void purge() {
        log.debug("Purging expired locks");
        log.debug("Current Size of locks: " + locks.size());
        Iterator<Map.Entry<String, TimedLock>> iterator = locks.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, TimedLock> entry = iterator.next();
            if (entry.getValue().isExpired()) {
                log.debug("Removing lock after expiry:" + entry.getKey());
                iterator.remove();
            }
        }

        log.debug("Lock size after purge: " + locks.size());
        lastPurge = System.currentTimeMillis();
    }

    synchronized void setPurgeInterval(long purgeInterval) {
        this.purgeInterval = purgeInterval;
    }

    @Override
    public boolean hasLock(String name) {
        TimedLock lockHolder = locks.get(name);
        return lockHolder != null;
    }

    public int getLockSize() {
        return locks.size();
    }
}
