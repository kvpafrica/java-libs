package co.onefi.services.lock.impl;

import co.onefi.services.lock.api.LockAcquisitionException;
import co.onefi.services.lock.api.LockContainer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
/**
 *
 * @author taiwo
 */
@Slf4j
public class LockWrapper {

    private final Lock lock;
    private final String name;
    

    public LockWrapper(Lock lock, String name) {
        this.lock = lock;
        this.name = name;
    }

    public <T> T tryLockAndExecute(Callable<T> callable) {
        return tryLockAndExecute(callable, 0);
    }

    public <T> T tryLockAndExecute(Callable<T> callable, long timeout) {
        try {
            boolean locked = timeout > 0 ? lock.tryLock(timeout, TimeUnit.MILLISECONDS) : lock.tryLock();
            if (locked) {
                try {
                    return callable.call();
                } catch (Exception e) {
                    if (e instanceof RuntimeException) {
                        throw (RuntimeException) e;
                    }
                    throw new RuntimeException(e);
                } finally {
                    unlockTransactionally();
                }
            } else {
                throw new LockAcquisitionException("Could not acquire lock for after timeout: " + name);
            }
        } catch (InterruptedException ie) {
            throw new LockAcquisitionException("Lock acquisition interrupted, " + name, ie);
        }
    }

    public void unlockTransactionally() {
        if (TransactionSynchronizationManager.isActualTransactionActive()) {
            log.debug("{}: Currently in transaction, unlock deferred", name);
            TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronization() {
                @Override
                public void afterCompletion(int status) {
                    log.debug("{}: Unlocking after transaction completed status, = {}", name, status);
                    lock.unlock();
                }
            });
        } else {
            log.debug("{}: Lock released", name);
            lock.unlock();
        }
    }

    public static LockWrapper getWrapper(LockContainer container, String key, long timeoutInMills) {
        Lock lock = container.getLock(key, timeoutInMills);
        return new LockWrapper(lock, key);
    }
}
