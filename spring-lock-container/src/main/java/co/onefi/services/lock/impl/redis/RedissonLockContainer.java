package co.onefi.services.lock.impl.redis;

import co.onefi.services.lock.api.LockAcquisitionException;
import co.onefi.services.lock.api.LockContainer;
import java.util.concurrent.locks.Lock;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;

/**
 *
 * @author taiwo
 */
@Slf4j
public class RedissonLockContainer implements LockContainer {

    final RedissonClient client;
    final String lockPrefix;

    public RedissonLockContainer(RedissonClient client, String lockPrefix) {
        this.client = client;
        this.lockPrefix = lockPrefix;
    }

    String getKey(String name) {
        return String.format("%s-%s", lockPrefix, name);
    }

    @Override
    public Lock getLock(String name, long ttl) throws LockAcquisitionException {
        log.debug("Getting key: " + name + " with ttl: " + ttl);
        String key = getKey(name);
        RLock lock = client.getFairLock(key);
        return new RedissonLockDelegate(lock, ttl);
    }

    @Override
    public boolean hasLock(String name) {
        return false;
    }

}
