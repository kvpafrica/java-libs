package co.onefi.services.lock.config;

import co.onefi.services.lock.api.LockContainer;
import co.onefi.services.lock.impl.redis.RedissonLockContainer;
import co.onefi.services.lock.impl.standard.StandardJavaLockContainer;
import org.apache.commons.lang3.StringUtils;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.ReplicatedServersConfig;
import org.redisson.config.SentinelServersConfig;
import org.redisson.config.SingleServerConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author taiwo
 */
@Configuration
@EnableConfigurationProperties(value = {RedisProperties.class})
public class LockConfiguration {

    @Value("${lock.container.prefix:redis-lock}")
    private String lockPrefix;

    @Bean
    @ConditionalOnProperty(name = "lock.container.type", havingValue = "redis")
    public LockContainer redisLockContainer(RedisProperties redisProperties) {
        Config config = new Config();
        if (redisProperties.getSentinel() != null) {
            if (StringUtils.isEmpty(redisProperties.getSentinel().getMaster())) {
                ReplicatedServersConfig serversConfig = config.useReplicatedServers();
                serversConfig.addNodeAddress(redisProperties.getSentinel().getNodes().toArray(String[]::new));
                if (redisProperties.getPassword() != null) {
                    serversConfig.setPassword(redisProperties.getPassword());
                }
                serversConfig.setConnectTimeout(redisProperties.getTimeout().toMillisPart());
            } else {
                SentinelServersConfig sentinelServersConfig = config.useSentinelServers();
                sentinelServersConfig.setMasterName(redisProperties.getSentinel().getMaster());
                if (redisProperties.getSentinel().getNodes() != null && !redisProperties.getSentinel().getNodes().isEmpty()) {
                    sentinelServersConfig.addSentinelAddress(redisProperties.getSentinel().getNodes().toArray(String[]::new));
                }
                sentinelServersConfig.setDatabase(redisProperties.getDatabase());
                if (redisProperties.getPassword() != null) {
                    sentinelServersConfig.setPassword(redisProperties.getPassword());
                }
            }
        } else { //single server
            SingleServerConfig singleServerConfig = config.useSingleServer();
            // format as redis://127.0.0.1:7181 or rediss://127.0.0.1:7181 for SSL
            String schema = redisProperties.isSsl() ? "rediss://" : "redis://";
            singleServerConfig.setAddress(schema + redisProperties.getHost() + ":" + redisProperties.getPort());
            singleServerConfig.setDatabase(redisProperties.getDatabase());
            if (redisProperties.getPassword() != null) {
                singleServerConfig.setPassword(redisProperties.getPassword());
            }
        }
        RedissonClient redissonClient = Redisson.create(config);
        return new RedissonLockContainer(redissonClient, lockPrefix);
    }

    @Bean
    @ConditionalOnProperty(name = "lock.container.type", havingValue = "standard", matchIfMissing = true)
    public LockContainer lockContainer() {
        return new StandardJavaLockContainer();
    }
}
