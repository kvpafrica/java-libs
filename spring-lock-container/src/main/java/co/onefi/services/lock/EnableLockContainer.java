package co.onefi.services.lock;

import co.onefi.services.lock.config.LockConfiguration;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.context.annotation.Import;

/**
 *
 * @author taiwo
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(LockConfiguration.class)
@Documented
public @interface EnableLockContainer {
    
    String value() default "";
    
}


