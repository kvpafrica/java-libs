package co.onefi.services.lock.api;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 *
 * @author taiwo
 */
public abstract class TimedDelegatedLock implements Lock {

    protected Lock lock;
    protected long timeout;

    public TimedDelegatedLock(Lock lock, long timeout) {
        this.lock = lock;
        this.timeout = timeout;
    }

    @Override
    public void lock() {
        lock.lock();
        updateExpiry();
    }

    abstract protected void updateExpiry();

    @Override
    public void lockInterruptibly() throws InterruptedException {
        lock.lockInterruptibly();
        updateExpiry();
    }

    @Override
    public boolean tryLock() {
        if (lock.tryLock()) {
            updateExpiry();
            return true;
        }

        return false;
    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        if (lock.tryLock(time, unit)) {
            updateExpiry();
            return true;
        }

        return false;
    }

    @Override
    public void unlock() {
        lock.unlock();
    }

    @Override
    public Condition newCondition() {
        return lock.newCondition();
    }
}
