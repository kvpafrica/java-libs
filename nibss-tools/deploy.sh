#!/bin/bash
export APP_DIR=`dirname $0`
cd $APP_DIR
export DEPLOY_GIT="git:releases_via_java-libs://git@bitbucket.org:kvpafrica/java-libs.git"

mvn deploy:deploy-file \
        -DgroupId=com.didisoft.pgp \
        -DartifactId=pgplib \
        -Dversion=2.5 \
        -Dfile="${APP_DIR}/lib/pgplib-2.5.jar" \
        -DgeneratePom=true \
        -DrepositoryId=onefi-bb \
        -Durl=$DEPLOY_GIT

mvn deploy:deploy-file \
        -DgroupId=nfp.ssm.core \
        -DartifactId=ssm-module \
        -Dversion=2.0-ONEFI \
        -Dfile="${APP_DIR}/lib/ssm-module-2.0.jar" \
        -DpomFile="${APP_DIR}/lib/pom-ssm.xml" \
        -DrepositoryId=onefi-bb \
        -Durl=$DEPLOY_GIT

