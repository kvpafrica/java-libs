def mvn(args) {
    sh "mvn ${args}"
}

def version() {
    def matcher = readFile('pom.xml') =~ '<version>(.+)-.*</version>'
    matcher ? matcher[0][1].tokenize(".") : null
}

// Configure the Target JDK Settings here, as defined on the "Global Tool Configuration" page on Jenkins.
def MY_TARGET_JDK_NAME = "Zulu-JDK-17"
def MY_TARGET_JDK_HOME_SUB_DIR = "zulu17.38.21-ca-jdk17.0.5-linux_x64"
def MY_JDK_TOOL_PATH = ""   // This will be set later at each stage.
def COMMIT_HASH = ""    // This will be set later at each stage.

pipeline {
    agent any

    tools {
        jdk "${MY_TARGET_JDK_NAME}"
        maven 'MAVEN_HOME'
    }

    stages {
        stage ('In-Progress Notification') {
            steps {
                bitbucketStatusNotify ( buildState: 'INPROGRESS' )
            }
        }
        stage('Checkout') {
            steps {
                checkout scm
                script {
                    COMMIT_HASH = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%h'").trim()
                }
                echo "Commit Hash => ${COMMIT_HASH}"
            }
        }
	    stage('Test Project') {
            steps {
                script {
                    MY_JDK_TOOL_PATH = tool "${MY_TARGET_JDK_NAME}"
                }
                // Source: https://github.com/jenkinsci/pipeline-examples/blob/master/pipeline-examples/maven-and-jdk-specific-version/mavenAndJdkSpecificVersion.groovy
                withEnv(["JAVA_HOME=${MY_JDK_TOOL_PATH}/${MY_TARGET_JDK_HOME_SUB_DIR}", "MY_JAVA_PATH=${MY_JDK_TOOL_PATH}/${MY_TARGET_JDK_HOME_SUB_DIR}/bin"]) {
                    sh (" echo Java Home is: \$JAVA_HOME")
                    sh (" echo My Java Path is: \$MY_JAVA_PATH")
                    sh (" ls \$MY_JAVA_PATH")
                    sh 'mvn -version'
                    sh "mvn clean test"
                }
            }
        }
        stage('Build') {
            steps {
                script {
                    MY_JDK_TOOL_PATH = tool "${MY_TARGET_JDK_NAME}"
                }
                // Source: https://github.com/jenkinsci/pipeline-examples/blob/master/pipeline-examples/maven-and-jdk-specific-version/mavenAndJdkSpecificVersion.groovy
                withEnv(["JAVA_HOME=${MY_JDK_TOOL_PATH}/${MY_TARGET_JDK_HOME_SUB_DIR}", "MY_JAVA_PATH=${MY_JDK_TOOL_PATH}/${MY_TARGET_JDK_HOME_SUB_DIR}/bin"]) {
                    sh (" echo Java Home is: \$JAVA_HOME")
                    sh (" echo My Java Path is: \$MY_JAVA_PATH")
                    sh (" ls \$MY_JAVA_PATH")
                    sh 'mvn -version'
                    sh "mvn clean package -DskipTests"
                }
            }
        }
    }

    post{
      always{
        echo "Finalising Build..."
      }
      success{
        bitbucketStatusNotify ( buildState: 'SUCCESSFUL' )
      }
  }
}